import csv
import io
import os
import re
import numpy as np
import pandas as pd


# Sort nicely
def tryint(s):
    try:
        return int(s)
    except:
        return s

def alphanum_key(s):
    """ Turn a string into a list of string and number chunks.
        "z23a" -> ["z", 23, "a"]
    """
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def sort_nicely(l):
    """ Sort the given list in the way that humans expect.
    """
    l.sort(key=alphanum_key)





history_dir = 'C:/Users/User/Documents/Paradox Interactive/Hearts of Iron IV/mod/blackice_v4.0/history/states/'
files = os.listdir(history_dir)
sort_nicely(files)

csv_path = 'C:/Users/User/Documents/Paradox Interactive/Hearts of Iron IV/mod/blackice_v4.0/BICE_modfile/resources.csv'

detail_list = ['State_ID', 'State_Name', 'State_Owner']
resource_list = ['coal', 'iron', 'bauxite', 
                'tungsten', 'chromium', 'oil', 'rubber',
                'steel', 'aluminium',
                'food']
building_list = ['infrastructure']
# building_list = ['infrastructure', 'industrial_complex', 'arms_factory','dockyard',
#             'anti_air_building', 'air_base', 'radar_station', 'rocket_site', 'nuclear_reactor',
#             'shipyard_capital_building', 'tank_assembly', 'air_assembly', 'truck_factory', 'artillery_factory', 'smallarms_factory','uniform_factory', 'shipyard', 'subyard',
#             'fuel_silo', 'synthetic_refinery', 'synthetic_rubber_refinery', 'steel_refinery', 'aluminium_refinery',
#             'hydro_power', 'power_plant',
#             'rubber_storage', 'aluminium_storage', 'tungsten_storage', 'steel_storage', 'chromium_storage',
# 			'recruitment_building', 'police_station', 'research_centre',
#             'Province_Buildings']
#province_buildings = ['pillbox', 'bunker', 'naval_base', 'coastal_bunker' ]



def text_to_csv(a=0,b=10000):
    fieldnames = []
    rows = {}
    resourcevalues = {}
    buildingvalues = {} 

    for detail in detail_list:
        fieldnames.append(detail)
        rows.update({detail:detail})
    for resource in resource_list:
        fieldnames.append(resource)
        rows.update({resource:resource})
    for building in building_list:
        fieldnames.append(building)
        rows.update({building:building})

    with open(csv_path, 'w', newline='') as f:
        # fieldnames = ['State_ID', 'State_Name', 'coal', 'iron', 'bauxite', 'tungsten', 'chromium', 'oil', 'rubber', 'infrastructure', 'industrial_complex', 'arms_factory','dockyard', 'air_base']
        thewriter = csv.DictWriter(f, fieldnames=fieldnames)
        thewriter.writerow(rows)
        nr = 0
        for file_name in files[a:b]:
            foundbuildings = False
            nr+=1
            state_id = 0
            state_owner = ''
            state_name = re.search("[\s \w \- ' .]+.txt", file_name[re.search("-", file_name).end():]).group()
            state_name = state_name[:-4]

            # coal = ''
            # iron = ''
            # bauxite = ''
            # tungsten = ''
            # chromium = ''
            # oil = ''
            # rubber = ''
            # infrastructure = ''
            # industrial_complex = ''
            # arms_factory = ''
            # dockyard = ''
            # air_base = ''

            for resource in resource_list:
                resourcevalues.update({resource:0})
            for building in building_list:
                if(building == 'Province_Buildings'):
                    buildingvalues.update({building:""})
                else:
                    buildingvalues.update({building:0})



            with open(history_dir + file_name, 'r') as file:
                # read a list of lines into data
                data = file.readlines()
                linenr = 0
                depth = 0
                inbuild = False
                for line in data:
                    linenr += 1

                    line_figured_out = False
                    original_line = line
                    if(re.search("[#]", line)):
                        line = line[:re.search("[#]", line).end()-1]

                    if(re.search("buildings[\s]*=[\s]*{", line)):
                        depth = 0
                        inbuild = True
                        line_figured_out = True
                        if(foundbuildings):
                            break
                        foundbuildings = True


                    if (re.search("{", line)):
                        depth +=1
                    if (re.search("}", line)):
                        depth -=1
                        inres = False
                        if (depth == 0):
                            inbuild = False


                    if(re.search("[\s]+id[\s]*=", line)):
                        state_id = int(''.join(filter(lambda i: i.isdigit(), line)))

                    if(state_owner == ''):
                        if(re.search('owner[\s]*=', line) and re.search('[A-Z][A-Z][A-Z]', line)):
                            state_owner = re.search('[A-Z][A-Z][A-Z]', line).group()


                    for resource in resource_list:
                        if resource in line:
                            tempval = int(re.search('[\d]+', line).group())
                            resourcevalues.update({resource: tempval})
                
                    for building in building_list:
                        if (re.search(f'[\s]+{building}', line)):
                            line_figured_out = True
                            try:
                                tempval = int(''.join(filter(lambda i: i.isdigit(), line)))
                                buildingvalues.update({building: tempval})
                            except:
                                print(f'fail at {state_id}-{state_name} on line {linenr}')
                    if(re.search("history", line) and not re.search("{", line)):
                        print(f'it occurs in {state_id}-{state_name}')
            # 
            # if(buildingvalues['Province_Buildings'] == ""):
            #     buildingvalues.update({'Province_Buildings': 'None'})
            #     # print(state_id, state_name)

            endrow= dict({'State_ID': state_id, 'State_Name': state_name, 'State_Owner' : state_owner}, **resourcevalues, **buildingvalues)
            thewriter.writerow(endrow)
    print(nr)

def csv_to_txt():
    
    #### Get every file
    
    # files = []
    
    # directory = os.fsencode(history_dir)
    # 
    # for file in os.listdir(directory):
    #     filename = os.fsdecode(file)
    #     # if filename.endswith(".txt"):
    #     files.append(os.path.join(history_dir, filename))
    
    #### PD Data Frame
    
    df = pd.read_csv(csv_path, low_memory=False)
    
    # Debug:
    # print(df)
    
    #### Iterate through every row
    
    for i in df.index:
        #### Find ID and name
    
        id = int(df["State_ID"][i])
        name = df["State_Name"][i]
    
        #### Generate resource list
    
        resources = ""
    
        for c in resource_list:
            if (df[c][i] > 0):
                resources += (f'\t\t{c} = {int(df[c][i])}\n')
    
    

    
        # print(resources)
        # print(buildings)
        #### Find state File
        for file in files:
            if (re.search(f'{id}-{name}.txt', file)):
                state = file
                break
            if (re.search(f'{id} -{name}.txt', file)):
                state = file
                break
        if(not re.search(name, state)):
            print(f'state {name} defaulted to {state}')
    
    
    
        #### Read all the files
        # print("ok")
        read = open(history_dir + file, "r", encoding="utf-8")
        
        lines = []
        
        for line in read:
            lines.append(line)
        
        read.close()
        
        write = open(history_dir + state, "w", encoding="utf-8")
        
        #### Re-Write
        
        done = False
        builddone = False
        inres = False
        inbuild = False
        intime = False
        depth = 0
        
        for line in lines:
        
            if (re.search("resources", line)):
                inres = True

            if (not inres):
                write.write(line)
            if (re.search("{", line)):
                depth +=1
            if (re.search("}", line)):
                depth -=1
                inres = False
                if (depth == -1):
                    inbuild = False
                    intime = False
                    
                                    
            if (not done):
                if (re.search("{", line)):
                    if (resources != ""):
                        write.write(
                            # f'\n'
                            f'\tresources = {{\n'
                            f'{resources}'
                            f'\t}}\n'
                            # f'\n'
                        )
                    done = True
            # write.write(name)
        
        write.close()


# text_to_csv(490, 500)
# text_to_csv()
csv_to_txt()