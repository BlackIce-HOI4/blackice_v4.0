﻿capital = 302

oob = "BOL_1936"

set_war_support = 0.15
set_convoys = 5
set_country_flag = league_member
set_country_flag = olympic_attendee

country_event = { id = bolivia.1 days = 137 }

# Starting tech
set_technology = {
	
	#BICE_airrange_balance = 1
	################# infantry ###############
	tech_infantry_uniforms = 1
	infantry_weapons = 1
	tech_Garrison = 1
	tech_Headquarters = 1
	################## support ##################
	tech_recon = 1
	subtech_recon_cav_1 = 1
	tech_support = 1		
	tech_engineers = 1
	jungle_equipment = 1
	################## artillery ###############
	gw_artillery = 1
	################## aircraft ##############
	early_fighter = 1
	generic_fighter = 1
	generic_bomber = 1
	generic_strategic_bomber = 1
	early_CAS = 1
	################# ships ################
	generic_naval_tech = 1
	generic_naval = 1
	############### WW1 doctrines ############
	ww1_infantry = 1
	infantry_charge = 1
	complex_trench_systems = 1
	offensive_trenches = 1
	defensive_trenches = 1
	reserve_trenches = 1
	communication_trenches = 1
	logistics_trenches = 1	
	ww1_artillery = 1
	foot_runners = 1
	
	mobile_doctrines = 1
	cavalry_charges = 1
	cavalry_dismounting = 1
	
	ww1_battlefield_support = 1
}

add_ideas = { 	
	export_focus
	tax_low
	tariffs_none
	
	civilian_economy
	press_regulated
	key_industries
	labor_none
	fdi_allowed
	
	cons_volunteer
	foreign_volunteers
	mob_reserve
	train_none
	age_19
	officer_train_none
	security_minor_restrictions
	edu_minimal

	foreign_interventionism
	neutrality_idea
	CW_great_depression_3
	CHI_army_corruption_1
	poor_diet
}
if = {
	limit = {
		has_dlc = "Waking the Tiger"
	}
	add_ideas = {
		CHI_incompetent_officers
	}
}
add_manpower = 3000

set_country_flag = neutrality_idea

set_country_flag = monroe_doctrine

add_opinion_modifier = { 
	target = PAR
	modifier = BFTB_incongruous_neighbors
}
set_politics = {
	ruling_party = liberalism
	last_election = "1934.11.11"
	election_frequency = 72
	elections_allowed = yes
}

set_popularities = {
	conservatism = 1
	fascism = 2
	communism = 8
	liberalism = 35
	socialism = 25
	agrarianism = 19
	neutrality = 10
}
recruit_character = BOL_jose_luis_tejada_sorzano
recruit_character = BOL_oscar_unzaga_de_la_vega
recruit_character = BOL_rojas_ballivian
recruit_character = BOL_guiterrez_monje
recruit_character = BOL_basutto_arancha
recruit_character = BOL_miguel_melgarejo
recruit_character = BOL_diego_melgarejo
recruit_character = BOL_juan_francisco_de_la_cruz
recruit_character = BOL_esteban_de_la_cruz
recruit_character = BOL_eduardo_zapata
recruit_character = BOL_ladislao_gamarra
recruit_character = BOL_eusebio_ayala
recruit_character = BOL_tomas_monje_gutierrez
recruit_character = BOL_gil_ibarra_juarez
recruit_character = BOL_gualberto_villarroel_lopez
recruit_character = BOL_jose_santos_quinteros
recruit_character = BOL_fabian_vaca_chavez
recruit_character = BOL_david_toro_ruilova
recruit_character = BOL_enrique_baldivieso_aparicio
recruit_character = BOL_roberto_hinojosa
recruit_character = BOL_carlos_altamirano
recruit_character = BOL_julian_montellano_carrasco
recruit_character = BOL_hector_bilbao_roja
recruit_character = BOL_alberto_ostria_gutierrez
recruit_character = BOL_german_busch_beccera
recruit_character = BOL_jose_maria_gutierrez
recruit_character = BOL_enrique_finot
### EOF ###