﻿capital = 271

oob = "ETH_1936"

set_convoys = 5

set_stability = 0.7
set_country_flag = league_member

if = {
	limit = {
		not = { has_dlc = "Man the Guns" }
	}
	set_technology = {
		generic_naval_tech = 1
		generic_naval = 1
	}
}
if = {
	limit = {
		has_dlc = "Man the Guns"
	}
	set_technology = {
		early_ship_hull_light = 1
		#early_ship_hull_submarine = 1
	}
}

# Starting tech
set_technology = {
	
	building_tech = 1
	tech_basic_train = 1
	################# infantry ###############
	infantry_weapons = 1
	hmg = 1
	HMG_equipment_infantry_group_tech = 1
	#tech_mountaineers = 1
	tech_Headquarters = 1
	tech_support = 1
	camelry = 1
	################## support ##################
	tech_recon = 1
	subtech_recon_cav_1 = 1
	desert_equipment = 1
	################## aircraft ##############
	generic_fighter = 1
	generic_bomber = 1
	generic_strategic_bomber = 1
	############### WW1 doctrines ############
	ww1_infantry = 1
	infantry_charge = 1
	small_unit_raids = 1
	
	complex_trench_systems = 1
	defensive_trenches = 1
	basic_MG_defences = 1

	ww1_artillery = 1
	
	mobile_doctrines = 1
	cavalry_charges = 1
	terrain_mapping = 1

	ww1_battlefield_support = 1
}

add_ideas = { 	
	ETH_ancient_army

	export_focus
	tax_low
	tariffs_average
	
	war_economy
	press_regulated
	key_industries
	labor_none
	fdi_encouraged
	
	cons_expanded_draft
	foreign_volunteers
	mob_general
	train_none
	age_16
	officer_train_none
	security_minor_restrictions
	edu_minimal
	foreign_isolationism
	mountainous_country
	subsistence_diet
}

set_politics = {
	ruling_party = monarchism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	monarchism = 96
	conservatism = 1.9
	agrarianism = 1.1
	fascism = 0
	communism = 1
	neutrality = 0
}
add_opinion_modifier = {
    target = SOV
    modifier = past_sale_3
}
recruit_character = ETH_haile_selassie
recruit_character = ETH_fitawrari_yeebio_woldai
recruit_character = ETH_seyoum_mengesha
recruit_character = ETH_bekele_mikael
recruit_character = ETH_demissie_seged
recruit_character = ETH_kelile_makonnen
recruit_character = ETH_demissie_melekot
recruit_character = ETH_berhanu_makonnen
recruit_character = ETH_demissie_mikael
recruit_character = IEA_luigi_frusci
recruit_character = IEA_Alfredo_siniscalchi
recruit_character = IEA_carlo_geloso
recruit_character = IEA_enrico_cerulli
recruit_character = ETH_makonnen_endelkatchew
recruit_character = ETH_bedjironde_tekle_hawariate
recruit_character = IEA_emilio_faldella
recruit_character = IEA_vezio_lucchini
recruit_character = ETH_ras_imru_haile_selassie
recruit_character = IEA_augusto_turati
recruit_character = IEA_ugo_villarde
recruit_character = IEA_francesc_caroselli
recruit_character = ETH_hamid_awate
recruit_character = IEA_guglielmo_nasi
recruit_character = IEA_siad_barre
recruit_character = IEA_Giuseppe
recruit_character = IEA_Ferlesch
recruit_character = IEA_Aldo

###EOF###