﻿capital = 608

#set_cosmetic_tag = HCPC
oob = "PGR_1936"

recruit_character = PGR_song_zheyuan
recruit_character = PGR_zhao_dengyu
recruit_character = PGR_wan_fulin
recruit_character = PGR_liu_chensan
recruit_character = PGR_tong_linge
recruit_character = PGR_wang_shuchang
recruit_character = PGR_wang_kemin
recruit_character = PGR_jim_walker
recruit_character = PGR_alex_smith
recruit_character = PGR_marcus_taylor
recruit_character = PGR_jonas_brown
recruit_character = PGR_bob_wilson
recruit_character = PGR_jonas_white

if = {
	limit = {
		not = { has_dlc = "Man the Guns" }
	}
	set_technology = {
		generic_naval_tech = 1
		generic_naval = 1
	}
}

if = {
	limit = {
		has_dlc = "Man the Guns"
	}
	set_technology = {
		early_ship_hull_light = 1
		#early_ship_hull_coastal_submarine = 1
	}
}
set_technology = {
	
	building_tech = 1
	tech_basic_train = 1
	chinese_units = 1
	tech_infantry_uniforms = 1
	infantry_weapons = 1
	tech_Garrison = 1
	tech_Headquarters = 1
	tech_support = 1
	tech_recon = 1
	subtech_recon_cav_1 = 1
	ww1_infantry = 1
	infantry_charge = 1
	small_unit_raids = 1
	complex_trench_systems = 1
	defensive_trenches = 1
	ww1_artillery = 1
	artillery_concentration = 1
	basic_MG_defences = 1
	multiple_MG_nests = 1
	foot_runners = 1
	mobile_doctrines = 1
	cavalry_charges = 1
	cavalry_dismounting = 1
	armored_car_recon = 1
	motorcycle_liaison = 1
	staff_officer_vehicles = 1
	armor_support = 1
	ww1_battlefield_support = 1
	terrain_mapping = 1
	first_aid_stations = 1
	WW1_air_power = 1
	nrm_pre_ww1_naval_doctrine = 1
	nrm_jeune_ecole_concepts = 1
	nrm_destroyer_support = 1
	generic_fighter = 1
	generic_bomber = 1
	generic_strategic_bomber = 1
	
}

add_ideas = { 	
	export_focus
	tax_low
	tariffs_average
	
	civilian_economy
	press_regulated
	key_industries
	labor_none
	fdi_allowed
	
	cons_volunteer
	foreign_volunteers
	mob_reserve
	train_none
	age_19
	officer_train_none
	security_minor_restrictions
	edu_minimal
	foreign_isolationism
	neutrality_idea
	incompetent_industry_real
	incompetent_industry_real_navy
	KMT_weak_army	
	CHI_nine_power_treaty
	CHI_chinese_tribute
	subsistence_diet
}

if = {
	limit = {
		has_dlc = "Waking the Tiger"
	}
	add_ideas = {
		PRC_government_corruption
		CHI_ineffective_bureaucracy_warlord
		CHI_ineffective_bureaucracy
	}
}
set_politics = {	
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	fascism = 35
	neutrality = 55
	monarchism = 2
	agrarianism = 8
}