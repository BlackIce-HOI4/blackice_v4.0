﻿capital = 327

oob = "PHI_1936"

set_war_support = 0.15
set_country_flag = olympic_attendee

set_convoys = 5

if = {
	limit = {
		not = { has_dlc = "Man the Guns" }
	}
	set_technology = {
		generic_naval_tech = 1
		generic_naval = 1
	}
}

if = {
	limit = {
		has_dlc = "Man the Guns"
	}
	set_technology = {
	}
}

# Starting tech
set_technology = {
	
	building_tech = 1
	tech_basic_train = 1
	################# infantry ###############
	tech_Garrison = 1
	tech_Headquarters = 1
	infantry_weapons = 1
	################## support ##################
	tech_recon = 1
	subtech_recon_cav_1 = 1
	tech_support = 1		
	tech_engineers = 1
	jungle_equipment = 1
	################## artillery ###############
	################## aircraft ##############
	generic_fighter = 1
	generic_bomber = 1
	generic_strategic_bomber = 1
	############### WW1 doctrines ############
	ww1_infantry = 1
	infantry_charge = 1
	small_unit_raids = 1

	complex_trench_systems = 1
	defensive_trenches = 1

	ww1_artillery = 1
	foot_runners = 1
	
	basic_MG_defences = 1

	mobile_doctrines = 1
	cavalry_charges = 1
	cavalry_dismounting = 1
	cavalry_exploitation = 1
	motorcycle_liaison = 1
	staff_officer_vehicles = 1

	ww1_battlefield_support = 1
	first_aid_stations = 1
	battlefield_commissions = 1
	terrain_mapping = 1

	WW1_air_power = 1

}

add_ideas = { 	
	export_focus
	tax_low
	tariffs_average
	
	civilian_economy
	press_regulated
	key_industries
	labor_none
	fdi_allowed
	
	cons_volunteer
	foreign_volunteers
	mob_reserve
	train_none
	age_19
	officer_train_none
	security_minor_restrictions
	edu_minimal

	foreign_isolationism
	neutrality_idea
	poor_diet
}

set_politics = {
	ruling_party = conservatism
	last_election = "1935.9.15"
	election_frequency = 72
	elections_allowed = yes
}

set_popularities = {
	conservatism = 61
	liberalism = 14
	socialism = 17	
	fascism = 3
	communism = 5
}
#################################################################
recruit_character = PHI_manuel_luis_quezon
recruit_character = PHI_pedro_p._baguisa
recruit_character = PHI_ladislao_diwa
recruit_character = PHI_hermano_rizal
recruit_character = PHI_jacinto_bonifacio
recruit_character = PHI_valentine_aguainaldo
recruit_character = PHI_deodato_arellano
recruit_character = PHI_leon_plata
recruit_character = PHI_emilio_aguinaldo
recruit_character = PHI_juan_sumulong
recruit_character = PHI_sergio_osmena
recruit_character = PHI_benito_n_ebuen
recruit_character = PHI_victor_h_dizon
recruit_character = PHI_ramon_a_alcaraz
recruit_character = PHI_leoncio_s_tan
recruit_character = PHI_basilio_j_valdez
recruit_character = PHI_pelagio_cruz
recruit_character = PHI_edwin_andrews
recruit_character = PHI_santiago_nuval
recruit_character = PHI_eustacio_orobia
recruit_character = PHI_vicente_p_lim
recruit_character = PHI_heraclio_alano
recruit_character = PHI_basilio_fernando
recruit_character = PHI_jose_v_andrada

### EOF ###