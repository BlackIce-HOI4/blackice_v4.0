﻿capital = 617
set_convoys = 5
oob = "SIK_1936"
set_country_flag = SOV_air
set_war_support = 0.25
country_event = { id = xinjiang.1 days = 515 }
set_variable = { officers_purged_train = 0.38 }
set_variable = { officers_purged_mobilization = -0.70 }
set_variable = { officers_purged_command = -0.22 }
set_variable = { officers_purged_experience = -0.35 }
set_variable = { officers_purged_experience_cap = -80 }
set_variable = { officers_choice = 0 }

set_variable = { army_purge_defence = -0.05 }
set_variable = { army_purge_org = -0.05 }
set_variable = { army_purge_morale = -0.1 }
set_variable = { army_purge_breakthough = -0.1 }
set_variable = { army_purge_width = 0.1 }
set_variable = { army_purge_attrition = -5 }
set_variable = { army_purge_tank_defense = 0 }

set_variable = { air_purge_xp = -0.25 }
set_variable = { air_purge_xp_max = -25 }
set_variable = { air_purge_accident = 0.20 }
set_variable = { air_purge_ace = -0.15 }
set_variable = { air_purge_sup = 0 }
set_variable = { air_purge_cas = 0 }
set_variable = { air_purge_strat = -0.10 }
set_variable = { air_purge_nav = 0 }
set_variable = { air_purge_escort = 0 }
set_variable = { air_purge_intercept = 0 }
set_variable = { army_learns = 5 }
if = {
	limit = {
		not = { has_dlc = "Man the Guns" }
	}
	set_technology = {
		generic_naval_tech = 1
		generic_naval = 1
	}
}

if = {
	limit = {
		has_dlc = "Man the Guns"
	}
	set_technology = {
		early_ship_hull_light = 1
		#early_ship_hull_submarine = 1
	}
}

set_technology = {
	######### setup techs ##########
	
	#BICE_airrange_balance = 1
	building_tech = 1
	tech_basic_train = 1
	chinese_units = 1
	################# infantry ###############
	infantry_weapons = 1
	tech_Garrison = 1
	tech_Headquarters = 1
	################## support ##################
	tech_recon = 1
	subtech_recon_cav_1 = 1
	################## armor ##############
	#gwtank = 1
	################## aircraft ##############
	soviet_air_tech = 1
	################# ships ################
	generic_naval_tech = 1
	generic_naval = 1
	############### WW1 doctrines ############
	ww1_infantry = 1
	infantry_charge = 1
	complex_trench_systems = 1
	
	ww1_artillery = 1
	foot_runners = 1
	
	mobile_doctrines = 1
	cavalry_charges = 1
	cavalry_dismounting = 1
	ww1_battlefield_support = 1
	
	WW1_air_power = 1 #had received soviet air training since 32
	WW1_air_survey = 1
	WW1_air_bomb = 1
	
}

add_ideas = { 	
	
	civilian_economy
	press_regulated
	key_industries
	labor_none
	fdi_allowed
	
	cons_one
	foreign_volunteers
	mob_limited
	train_none
	age_19
	officer_train_none
	security_minor_restrictions
	edu_minimal
	foreign_isolationism
	neutrality_idea
	CHI_nine_power_treaty
	SIK_divided_nation
	CHI_ineffective_bureaucracy
	CHI_ineffective_bureaucracy_warlord
	incompetent_industry_real
	cg_warlords_ssc
	cu_is_warlord
	subsistence_diet
}

set_politics = {
	ruling_party = communism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	communism = 55
	conservatism = 7.5 
	liberalism = 1
	socialism = 4
	agrarianism = 0.5
	fascism = 5
	monarchism = 6.5
	neutrality = 20.5
}
###################################################################
recruit_character = SIK_sheng_shicai
recruit_character = SIK_osman_batur
recruit_character = SIK_ishaq_beg_munonov
recruit_character = SIK_muhammad_amin_bughra
recruit_character = SIK_chen_tanqiu
recruit_character = SIK_khoja_niaz
recruit_character = SIK_Ma_HuShan
recruit_character = SIK_pan_xiwang
recruit_character = SIK_zhong_genzhou
recruit_character = SIK_qin_wupen
recruit_character = SIK_hu_li
recruit_character = SIK_wan_niaoka
recruit_character = SIK_piotr_orlov
recruit_character = SIK_yu_wei
recruit_character = SIK_yang_tian
recruit_character = SIK_mao_zemin
recruit_character = SIK_cheng_fang
recruit_character = SIK_mahmud_sijan
recruit_character = SIK_ma_wei
recruit_character = SIK_yol_bars
recruit_character = SIK_kwong_tian
recruit_character = SIK_xie_peng
recruit_character = SIK_gao_jiang
recruit_character = SIK_mao_wu
recruit_character = SIK_hoja_niyaz
recruit_character = SIK_tan_ma
recruit_character = SIK_ma_shaowu
recruit_character = SIK_han_chang
recruit_character = SIK_isa_yusuf_alptekin
recruit_character = SIK_garegin_apresov
recruit_character = SIK_habibullah_huseinov
recruit_character = SIK_zhang_xiliang

### EOF ###