﻿capital = 322

oob = "TIB_1936"

set_stability = 0.65

set_war_support = 0.15
country_event = { id = himalaya_tibet.31 days = 5 }
country_event = { id = himalaya_tibet.24 days = 148 random = 20 }

# Starting tech
if = {
	limit = {
		not = { has_dlc = "Man the Guns" }
	}
	set_technology = {
		generic_naval_tech = 1
		generic_naval = 1
	}
}

if = {
	limit = {
		has_dlc = "Man the Guns"
	}
	set_technology = {
		early_ship_hull_light = 1
		#early_ship_hull_submarine = 1
	}
}
add_unit_bonus = {
	category_light_infantry = {
		mountain = {
			attack = 0.02
			movement = 0.10
		}
	}
}
set_technology = {
	Gurkha_tech = 1
	building_tech = 1
	tech_basic_train = 1
	tech_mountaineers = 1
	tech_Headquarters = 1
	infantry_weapons = 1
	tech_recon = 1
	subtech_recon_cav_1 = 1
	mountain_equipment = 1
	ww1_infantry = 1
	mobile_doctrines = 1
	ww1_battlefield_support = 1
	generic_fighter = 1
	generic_bomber = 1
	generic_strategic_bomber = 1

}
add_ideas = { 	
	TIB_mountainous_people
	TIB_poorly_organized_army
	TIB_monasteries_neutral
	TIB_backward_industry1
	
	export_focus
	tax_low
	tariffs_average
	
	isolation
	press_regulated
	key_industries
	labor_none
	fdi_allowed
	
	cons_volunteer
	foreign_volunteers
	mob_reserve
	train_none
	officer_train_none
	age_19
	security_minor_restrictions
	edu_minimal
	foreign_isolationism
	neutrality_idea
	subsistence_diet
	TIB_sci_idea
}

set_politics = {
	ruling_party = monarchism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	monarchism = 100
	conservatism = 0
	fascism = 0
	communism = 0
	neutrality = 0
}
###############################################################################
recruit_character = TIB_yutok_tashi_dondrub
recruit_character = TIB_changra_sapron
recruit_character = TIB_wangchen_geleg_surkhang
recruit_character = TIB_the_dalai_lama
recruit_character = TIB_jamphel_yeshe_gyaltsen
recruit_character = TIB_thubten_kunphela
recruit_character = TIB_bapa_phuntsog_wangyal
recruit_character = TIB_namgang_dazang_damdu
recruit_character = TIB_namgang_tsarong
recruit_character = TIB_penor_tendar
recruit_character = TIB_dundul_shakabpa
recruit_character = TIB_gendun_chophel
recruit_character = TIB_chao_kung
recruit_character = TIB_ngabo_ngawang_jigme
recruit_character = TIB_chama_samphe
recruit_character = TIB_ngawang_Sungrab_thutob
recruit_character = TIB_philip_neame
recruit_character = TIB_sogyal_rinpoche
recruit_character = TIB_gyato_wangdu
recruit_character = TIB_sampo_tsewang_rigzin
recruit_character = TIB_tendong_shappe
recruit_character = TIB_ngawang_kesang
recruit_character = TIB_lobsang_tenzin
recruit_character = TIB_lhalu_tsewang_dorje
recruit_character = TIB_taring_jigme
recruit_character = TIB_tenpa_jamyang
recruit_character = TIB_jampal_rabgye_rinpoche
recruit_character = TIB_bapa_yeshe
recruit_character = TIB_ENG_air_theorist
recruit_character = TIB_dorji_gyaltsen
recruit_character = TIB_phuntsog_rabgye
recruit_character = TIB_chodak_karma
recruit_character = TIB_chaghoe_namgyal_dorje
recruit_character = TIB_jamyang_rabten
recruit_character = TIB_chikyak_khenpo_lobsang_geleg
recruit_character = TIB_khemey_sonam_wangdi
recruit_character = TIB_jigme_taring
recruit_character = TIB_reting_rimpoche
recruit_character = TIB_taktra_rinpoche
recruit_character = TIB_tsarong_dzasa
recruit_character = TIB_rinzin_dorji
recruit_character = TIB_basil_gould

### EOF ###