
state = {
	resources = {
		food = 22
	}
	id = 10
	name = "STATE_10"
	
	manpower = 3089000

	local_supplies=0.0
	state_category = large_city

	history = {
		owner = POL
		buildings = {
			infrastructure = 7
			industrial_complex = 5
			arms_factory = 2
			anti_air_building = 2
			air_base = 5
			air_assembly = 2
		}

		set_variable = { vp_value = 2 } 
		set_variable = { highest_vp = 1 } 

		add_core_of = POL
		
		#set_variable = { state_taxes = 308 }

		
		victory_points = { 402 1 }
		victory_points = { 11492 1 }
	}

	provinces = {
		402 467 524 3482 6511 9400 11385 11492 12562 
	}
}
