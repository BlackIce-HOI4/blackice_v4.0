
state={
	resources = {
		food = 3
	}
	id=100
	name="STATE_100"
	
	manpower = 115870
	
	local_supplies=0.0
	state_category = rural

	history = {
		owner = DEN
		buildings = {
			infrastructure = 2
			industrial_complex = 2
			air_base = 1
			12674 ={
				naval_base = 1
			}
		}

		set_variable = { vp_value = 12 } 
		set_variable = { highest_vp = 10 } 

		add_core_of = DEN
		#add_core_of = ICE
		
		
		victory_points = { 4861 1 }
		victory_points = { 12674 10 }
		victory_points = { 12689 1 }
	}
	provinces = {
		4861 12674 12689
	}
}
