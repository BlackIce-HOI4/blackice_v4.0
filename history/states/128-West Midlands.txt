
state={
	resources = {
		coal = 20
		iron = 118
	}


	id=128
	name="STATE_128"
	
	manpower = 3685235
	
	local_supplies=0.0
	state_category = metropolis
	

	history = {
		owner = ENG
		buildings = {
			infrastructure = 8
			industrial_complex = 18
			arms_factory = 8
			air_base = 6
			tank_assembly = 2
			air_assembly = 3
			artillery_assembly = 2
			engine_assembly = 5
			steel_refinery = 6
			power_plant = 4
		}

		set_variable = { vp_value = 29 } 
		set_variable = { highest_vp = 25 } 

		add_core_of = ENG

		#set_variable = { state_taxes = 368 }
		
		
		
		1939.1.1 = {
			buildings = {
				arms_factory = 3
				industrial_complex = 4
			}
		}
		
		victory_points = { 3241 1 }
		victory_points = { 3301 1 }
		victory_points = { 6270 1 }
		victory_points = { 9297 25 }
		victory_points = { 11345 1 }
	}

	provinces={
		3241 3301 6270 6301 7239 9297 11345 
	}
}
