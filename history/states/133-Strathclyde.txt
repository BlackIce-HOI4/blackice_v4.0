
state={
	resources = {
		coal = 154
	}


	id=133
	name="STATE_133" # Lanark
	
	manpower = 2257723
	
	local_supplies=0.0
	state_category = city
	

	history = {
		owner = ENG
		buildings = {
			infrastructure = 7
			industrial_complex = 2
			dockyard = 5
			anti_air_building = 2
			air_base = 3
			shipyard_capital_building = 1
			tank_assembly = 2
			artillery_assembly = 2
			shipyard = 2
			steel_refinery = 4
			power_plant = 2
			6395 = {
				naval_base = 2
			}
		}

		set_variable = { vp_value = 17 } 
		set_variable = { highest_vp = 15 } 

		add_core_of = ENG

		#set_variable = { state_taxes = 225 }
		
		
		1939.1.1 = {
			buildings = {
				dockyard = 5
				industrial_complex = 2
			}
		}
		
		victory_points = { 3273 15 }
		victory_points = { 6395 1 }
		victory_points = { 11218 1 }
	}

	provinces = {
		3273 6320 6350 6385 6395 9237 9350 11218 
	}
}
