
state={
	resources = {
		food = 6
	}


	id=140
	name="STATE_140"

	history={
		owner = SWE
		buildings = {
			infrastructure = 6
			industrial_complex = 5
			arms_factory = 8
			dockyard = 1
			air_base = 1
			hydro_power = 2
			383 = {
				naval_base = 5
				coastal_bunker = 5
				bunker = 5

			}

		}

		set_variable = { vp_value = 14 } 
		set_variable = { highest_vp = 10 } 

		add_core_of = SWE
		victory_points = {
			295 1 
		}
		victory_points = {
			3375 1 
		}
		victory_points = {
			9054 1 
		}
		victory_points = {
			11114 1 
		}
		victory_points = {
			383 10 
		}

	}

	provinces={
		69 147 171 295 383 3063 3128 3141 3286 3375 3386 6028 6195 6310 6331 6406 9054 9138 9218 11070 11114 13096 
	}
	manpower=1282827
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=city
}
