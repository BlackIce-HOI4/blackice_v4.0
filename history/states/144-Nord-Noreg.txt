
state={
	resources = {
		iron = 60
		tungsten = 32
	}


	id=144
	name="STATE_144"

	history={
		owner = NOR
		buildings = {
			infrastructure = 3
			air_base = 1
			hydro_power = 2
			192 = {
				naval_base = 3

			}
			11050 = {
				naval_base = 2

			}

		}

		set_variable = { vp_value = 14 } 
		set_variable = { highest_vp = 10 } 

		add_core_of = NOR
		victory_points = {
			192 10 
		}
		victory_points = {
			3126 1 
		}
		victory_points = {
			3145 1 
		}
		victory_points = {
			9126 1 
		}
		victory_points = {
			11050 1 
		}

	}

	provinces={
		38 44 91 173 192 3034 3048 3058 3088 3126 3132 3145 6071 6111 6130 6187 6214 9126 9208 11050 11090 11157 
	}
	manpower=335251
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=rural
}
