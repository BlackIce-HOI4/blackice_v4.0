
state={
	resources = {
		food = 12
	}
	id=149
	name = "STATE_149"

	manpower = 811029
	
	local_supplies=0.0
	state_category = large_town #8 slots
	
	history={
		owner = FIN
		buildings = {
			infrastructure = 4
			industrial_complex = 10
			arms_factory = 4
			3122 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 8 } 
		set_variable = { highest_vp = 3 } 

		add_core_of = FIN
		
		victory_points = { 66 1 }
		victory_points = { 3122 3 }
		victory_points = { 158 3 }
		victory_points = { 3172 1 }
	}

	provinces={
		63 66 82 120 132 158 195 3119 3122 3172 6013 6081 6138 6141 6198 6212 9038 9156 9166 9219 11033 11041 11065 11081 11121 11126 11132 11144 13113 
	}
	
	buildings_max_level_factor = 1.000
}
