
state = {
	resources = {
		food = 41
	}


	id = 152
	name = "STATE_152"
	
	manpower = 2275377

	history = {
		owner = AUS
		buildings = {
			infrastructure = 6
			industrial_complex = 15
			arms_factory = 4
			aluminium_refinery = 2
			hydro_power = 2
		}

		set_variable = { vp_value = 4 } 
		set_variable = { highest_vp = 1 } 

		add_core_of = AUS

		#set_variable = { state_taxes = 227 }
		

		1938.3.12 = {
			owner = GER
			controller = GER
			add_core_of = GER
		}
		
		victory_points = { 3684 1 }
		victory_points = { 6708 1 }
		victory_points = { 6723 1 }
		victory_points = { 9665 1 }
	}

	provinces={
		732 3684 3703 6552 6708 6723 9665 11651 
	}
	
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=city
}
