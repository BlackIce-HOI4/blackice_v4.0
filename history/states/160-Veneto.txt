state={
	resources = {
		food = 17
	}


	id=160
	name="STATE_160"
	manpower=3405013 
	local_supplies=0.0
	state_category=large_city
	

	history={
		owner = ITA
		buildings = {
			infrastructure = 7
			industrial_complex = 13
			arms_factory = 6
			dockyard = 2
			air_base = 3
			steel_refinery = 1
			aluminium_refinery = 2
			hydro_power = 4
			11584 = {
				naval_base = 7
			}
		}

		set_variable = { vp_value = 14 } 
		set_variable = { highest_vp = 5 } 

		add_core_of = ITA

		#set_variable = { state_taxes = 396 }
		
		
		1939.1.1 = {
			buildings = {
				dockyard = 2
				industrial_complex = 5
			}
		}
		victory_points = { 603 3 }
		victory_points = { 6656 1 }
		victory_points = { 3604 5 }
		victory_points = { 11584 5 }
	}

	provinces={
	 3604 3657 6656 11584 603 9582
	}
	
	buildings_max_level_factor=1.000
	
}
