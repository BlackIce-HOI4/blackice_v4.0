
state={
	id=167
	name="STATE_167"
	
	manpower = 1979294
	
	local_supplies=0.0
	state_category = large_town

	history={
		owner = SPR
		buildings = {
			infrastructure = 6
			dockyard = 1
			air_base = 3
			6906 = {
				naval_base = 3
			}
			962 = {
				naval_base = 2
			}
		}

		set_variable = { vp_value = 14 } 
		set_variable = { highest_vp = 10 } 

		add_core_of = SPR
		
		
		1936.7.1 = {
			add_core_of = SPA
		}
		
		victory_points = { 962 1 }
		victory_points = { 4098 2 }
		victory_points = { 6817 1 }
		victory_points = { 6906 10 }
	}

	provinces={
		962 3799 3873 4098 6817 6856 6906 6959 9889 9896 
	}
}
