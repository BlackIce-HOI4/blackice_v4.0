
state={
	id=177
	name="STATE_177"
	manpower = 365500
	
	local_supplies=0.0
	state_category = small_island
	
	history={
		owner = SPR
		buildings = {
			infrastructure = 4
			air_base = 1
			9793 = {
				naval_base = 2
			}
			7114 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 3 } 
		set_variable = { highest_vp = 1 } 

		add_core_of = SPR
		
		
		victory_points = { 7114 1 }
		victory_points = { 9793 1 }
		victory_points = { 9845 1 }
	}
	provinces={
		7114 9793 9845
	}
}
