
state={
	id=178
	name="STATE_178"
	manpower = 561347
	
	local_supplies=0.0
	state_category = small_island

	history={
		owner = SPR
		buildings = {
			infrastructure = 3
			air_base = 1
			13071 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 1 } 
		set_variable = { highest_vp = 1 } 

		add_core_of = SPR
		
		
		1936.7.1 = {
			add_core_of = SPA
		}
		
		victory_points = { 13071 1 }
	}

	provinces={
		13071 
	}
}
