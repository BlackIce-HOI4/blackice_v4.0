
state={
	resources = {
		oil = 2
		food = 8
	}
	id=191
	name="STATE_191"
	history={
		owner = EST
		buildings = {
			infrastructure = 4
			air_base = 1
			4640 = {
				naval_base = 1

			}

		}

		set_variable = { vp_value = 2 } 
		set_variable = { highest_vp = 1 } 

		add_core_of = EST
		victory_points = {
			4640 1 
		}
		victory_points = {
			9221 1 
		}

	}

	provinces={
		592 4640 6099 6178 6408 9221 9485 11057 11443 13127 
	}
	manpower=376400
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=rural
}
