
state={
	resources = {
		food = 42
	}


	id=202
	name="STATE_202"

	history={
		owner = SOV
		buildings = {
			infrastructure = 5
			industrial_complex = 3
			arms_factory = 2
			air_base = 8
			power_plant = 2
			fuel_silo = 1
			3494 = {
				bunker = 2
			}
			504 = {
				bunker = 2
			}
			3543 = {
				bunker = 2
			}	
		}

		set_variable = { vp_value = 2 } 
		set_variable = { highest_vp = 1 } 

		add_core_of = SOV
		add_core_of = UKR
		victory_points = {
			504 1 
		}
		victory_points = {
			9568 1 
		}

	}

	provinces={
		489 504 582 3494 3543 9465 9543 9568 11557 
	}
	manpower=1795352
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=city
}
