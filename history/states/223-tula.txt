
state={
	id=223
	name="STATE_223"
	manpower = 1635549
	
	local_supplies=0.0
	state_category = town

	history={
		owner = SOV
		buildings = {
			infrastructure = 5
			arms_factory = 8
			air_base = 1
			steel_refinery = 2
		}

		set_variable = { vp_value = 4 } 
		set_variable = { highest_vp = 3 } 

		victory_points = {
			6262 3 
		}

		victory_points = { 3250 1 }
		add_core_of = SOV
		1939.1.1 = {
			buildings = {
				arms_factory = 3
			}
		}
	}

	provinces={
		212 261 300 3236 3250 3292 3376 6262 6315 6333 6396 9366 11242 11347 
	}
}
