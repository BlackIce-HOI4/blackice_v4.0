
state={
	resources = {
		food = 25
	}
	id=237
	name="STATE_237"
	manpower = 169620
	
	local_supplies=0.0
	state_category = rural

	history={
		owner = SOV
		buildings = {
			infrastructure = 2
		}

		set_variable = { vp_value = 1 } 
		set_variable = { highest_vp = 1 } 

		victory_points = { 11724 1 }
		add_core_of = SOV
	}

	provinces={
		481 742 745 759 3413 3439 3475 3597 3746 3774 6463 6791 6794 9426 9594 9720 9739 11566 11724 
	}
}
