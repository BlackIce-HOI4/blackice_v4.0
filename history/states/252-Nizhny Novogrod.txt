
state={
	id=252
	name="STATE_252"
	manpower = 3378176
	
	local_supplies=0.0
	state_category = city

	history={
		owner = SOV
		buildings = {
			infrastructure = 5
			industrial_complex = 3
			arms_factory = 10
			air_base = 1
			tank_assembly = 4
			air_assembly = 4
			artillery_assembly = 4
			engine_assembly = 4
			power_plant = 2
			fuel_silo = 1
		}

		set_variable = { vp_value = 11 } 
		set_variable = { highest_vp = 10 } 

		#set_variable = { state_taxes = 337 }
		victory_points = {
			11375 10
		}
		victory_points = { 6299 1 }
		add_core_of = SOV
		1939.1.1 = {
			buildings = {
				arms_factory = 3
			}
		}
	}

	provinces={
		12 256 270 297 368 3224 3245 3261 3285 3303 3349 3352 3383 6149 6231 6295 6299 6327 6370 6374 6392 6410 9291 9298 9325 9358 9360 9368 9389 9407 11275 11277 11324 11340 11344 11375 11394 
	}
}
