
state={
	resources = {
		oil = 4
	}


	id=293
	name="STATE_293"
	manpower = 2489500
	
	local_supplies=0.0
	state_category = rural
	
	history={
		owner = YEM
		buildings = {
			infrastructure = 3
			industrial_complex = 2
			arms_factory = 2
			10840 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 3 } 
		set_variable = { highest_vp = 3 } 

		#set_variable = { state_taxes = 248 }
		victory_points = {
			10840 3 
		}
		add_core_of = YEM
	}

	provinces={
		1920 1973 4924 10752 10840 
	}
}
