
state={
	resources = {
		iron = 52
	}
	id=30
	name="STATE_30"
	
	manpower = 1388400

	local_supplies=0.0
	state_category = city

	history={
		owner = FRA
		buildings = {
			infrastructure = 7
			industrial_complex = 5
			arms_factory = 4
			dockyard = 4
			air_base = 2
			shipyard_capital_building = 2
		}

		set_variable = { vp_value = 8 } 
		set_variable = { highest_vp = 5 } 

		add_core_of = FRA

		
		victory_points = { 3509 1 }
		victory_points = { 9478 1 }
		victory_points = { 11465 5 }
		victory_points = { 11613 1 }
	}

	provinces={
		503 527 3495 3509 3523 9478 9867 11463 11465 11613 11616 
	}
}
