
state={
	resources = {
		chromium = 4
	}


	id=317
	name="STATE_317"

	history={
		owner = NIC
		buildings = {
			infrastructure = 4
			industrial_complex = 5
			arms_factory = 4
			air_base = 1
			12738 = {
				naval_base = 1

			}

		}

		set_variable = { vp_value = 3 } 
		set_variable = { highest_vp = 3 } 

		add_core_of = NIC
		victory_points = {
			12738 3 
		}

	}

	provinces={
		1988 2075 10728 12738 13139 13178 
	}
	manpower=683000
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=town
}
