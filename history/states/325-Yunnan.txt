
state={
	resources = {
		tungsten = 6
	}


	id=325
	name="STATE_325"
	history={
		owner = YUN
		buildings = {
			infrastructure = 3
			industrial_complex = 8
			arms_factory = 2
			steel_refinery = 2
			hydro_power = 2
		}

		set_variable = { vp_value = 15 } 
		set_variable = { highest_vp = 15 } 

		add_core_of = YUN
		add_core_of = CHI
		add_core_of = PRC
		set_variable = {
			state_taxes = 1199
		}
		victory_points = {
			7446 15 
		}
		1938.10.25 = {
			owner = CHI
		add_core_of = CHI
		add_core_of = PRC

		}
	}
	provinces={
		1319 5072 7446 7606 10819 12282 12841 
	}
	manpower=11994500
	buildings_max_level_factor=1.500
	local_supplies=0.0
	state_category=town
}
