
state = {
	id=337
	name="STATE_337"
	manpower = 25900
	
	local_supplies=0.0
	state_category = small_island
	
	history = {
		owner = DEN
		buildings = {
			infrastructure = 4
			air_base = 1
			13003 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 1 } 
		set_variable = { highest_vp = 1 } 

		add_core_of = DEN
		
		
		victory_points = { 13003 1 }	
		
	}
	
	provinces = { 13003 }
}
