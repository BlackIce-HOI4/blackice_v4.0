
state={
	resources = {
		coal = 20
		food = 3
	}


	id=338
	name="STATE_338"
	
	manpower = 1900615
	
	local_supplies=0.0
	state_category = city
	

	history = {
		owner = ENG
		add_core_of = ENG

		buildings = {
			infrastructure = 7
			industrial_complex = 10
			arms_factory = 6
			dockyard = 1
			air_base = 2
			tank_assembly = 2
			air_assembly = 3
			engine_assembly = 4
			steel_refinery = 2
			power_plant = 4
			farm = 1
			fuel_silo = 1
			
			3369 = {
				naval_base = 2
			}
			
			9239 = {
				#Slough
				electronics_facility = 1
			}
		}

		set_variable = { vp_value = 9 } 
		set_variable = { highest_vp = 5 } 

		
		victory_points = { 3369 5 }
		victory_points = { 6351 1 }
		victory_points = { 9239 1 }
		victory_points = { 9484 1 }
		victory_points = { 11471 1 }
	}

	provinces = {
		3369 6221 6351 6378 9239 9484 11471 
	}
	
	
}
