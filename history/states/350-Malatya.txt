
state={
	resources = {
		food = 11
	}
	id=350
	name="STATE_350"

	history={
		owner = TUR
		buildings = {
			infrastructure = 3
			air_base = 2

		}

		set_variable = { vp_value = 4 } 
		set_variable = { highest_vp = 1 } 

		victory_points = {
			9893 1 
		}
		victory_points = { 4512 1 }
		victory_points = { 12416 1 }
		victory_points = { 4512 1 }
		add_core_of = TUR

	}

	provinces={
		4512 6813 7620 9823 9893 9912 11739 11759 11871 12387 12416 
	}
	manpower=1147156
	buildings_max_level_factor=1
	local_supplies=0.0
	state_category="rural"
}
