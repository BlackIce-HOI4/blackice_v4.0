
state={
	id=352
	name="STATE_352"

	history={
		owner = TUR
		buildings = {
			infrastructure = 2

		}

		set_variable = { vp_value = 2 } 
		set_variable = { highest_vp = 1 } 

		victory_points = { 6935 1 }
		victory_points = { 7530 1 }
		add_core_of = TUR

	}

	provinces={
		864 1463 6935 7530 10497 12318 12476 13121 
	}
	manpower=375490
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=rural
}
