
state={
	id=359
	name="STATE_359"
	manpower = 4112633


	local_supplies=0.0
	state_category = metropolis

	history={
		owner = USA
		add_core_of = USA
		
		buildings = {
			infrastructure = 7
			industrial_complex = 25
			dockyard = 3
			air_base = 4
			power_plant = 2

			6882 = {
				#Murray Hill
				electronics_facility = 1
			}
		}

		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 5 } 

		#set_variable = { state_taxes = 411 }
		victory_points = {
			6882 5
		}
	}

	provinces={
		3693 3863 6882 9801 9864 11740 11782 11900
	}

}
