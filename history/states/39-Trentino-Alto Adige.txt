state={
	resources = {
		iron = 5
	}

	id=39
	name="STATE_39" 
	
	manpower = 660137
	
	local_supplies=0.0
	state_category = town
	

	history = {
		owner = ITA
		buildings = {
			infrastructure = 6
			industrial_complex = 5
			air_base = 1
			hydro_power = 4
		}

		set_variable = { vp_value = 2 } 
		set_variable = { highest_vp = 1 } 

		add_core_of = ITA
		add_claim_by = AUS
		
		victory_points = { 9598 1 }
		victory_points = { 11598 1 }
	}

	provinces = {
		656 6631 6675 9598 9630 11598 11615
	}
}
