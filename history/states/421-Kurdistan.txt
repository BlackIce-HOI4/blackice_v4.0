
state={
	id=421
	name="STATE_421"
	manpower = 780278
	
	local_supplies=0.0
	state_category = rural

	history={
		owner = PER
		buildings = {
			infrastructure = 3
		}

		set_variable = { vp_value = 0 } 
		set_variable = { highest_vp = 0 } 

		add_core_of = PER
	}

	provinces={
		829 5050 5098 10774 12773 
	}
}
