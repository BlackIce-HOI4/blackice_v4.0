
state={
	resources = {
		iron = 90
		tungsten = 28
		food = 77
	}


	id=431
	name="STATE_431" # West Bengal
	manpower = 18778100
	
	local_supplies=0.0
	state_category = city
	

	history={
		owner = RAJ
		buildings = {
			infrastructure = 5
			industrial_complex = 10
			arms_factory = 6
			dockyard = 1
			air_base = 2
			steel_refinery = 2
			4245 = {
				naval_base = 6
			}
		}

		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 5 } 

		#set_variable = { state_taxes = 1877 }
		victory_points = {
			4245 5 
		}

	}

	provinces={
		1159 1609 1656 4062 4204 4245 4435 4644 7220 7475 7537 7565 7593 10077 12050 12338 
	}
}
