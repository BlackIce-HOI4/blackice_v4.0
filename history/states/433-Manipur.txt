
state={
	resources = {
		tungsten = 76
	}


	id=433
	name="STATE_433" # Rajasthan
	manpower = 11786000
	
	local_supplies=0.0
	state_category = town
	
	

	history={
		owner = RAJ
		buildings = {
			infrastructure = 3
			industrial_complex = 3
		}

		set_variable = { vp_value = 0 } 
		set_variable = { highest_vp = 0 } 

		#set_variable = { state_taxes = 1178 }

	}

	provinces={
		1199 1908 2054 4149 4915 4971 4984 7905 7998 10847 10866 12041 12718 12743 12829 12844 12876 12886 
	}
}
