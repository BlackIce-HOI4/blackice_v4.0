
state={
	id=462
	name="STATE_462"
	manpower = 1865552
	
	local_supplies=0.0
	state_category="pastoral" 

	history={
		owner = MAR
		buildings = {
			infrastructure = 2
			industrial_complex = 3
		}

		set_variable = { vp_value = 1 } 
		set_variable = { highest_vp = 1 } 

		victory_points = {
			1102 1
		}
		add_core_of = MAR
		#add_core_of = MOR #spanish morocco (MOR) shouldn't get the core imo because the french protectorate was more powerful and industrialized apparently
	}

	provinces={
		1102 1152 1207 1989 4022 4108 4154 4933 5046 7203 7908 7958 8046 10107 
	}
}
