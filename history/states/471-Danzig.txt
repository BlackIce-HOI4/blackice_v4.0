
state={
	id=471
	name="STATE_471"

	history={
		owner = POL
		buildings = {
			infrastructure = 6
			dockyard = 1
			air_base = 3
			power_plant = 2
			362 = {
				naval_base = 5

			}
		}

		set_variable = { vp_value = 15 } 
		set_variable = { highest_vp = 15 } 

		controller = POL
		
		victory_points = {
			362 15 
		}

	}

	provinces={
		362 
	}
	
	manpower=250371
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=city
}
