
state={
	id=474
	name="STATE_474"
	
	manpower = 440289
	
	local_supplies=0.0
	state_category = town 	#8 slots

	history = {
		owner = MEX
		buildings = {
			infrastructure = 3
			industrial_complex = 8
			arms_factory = 4
			air_base = 1
			#slots
			
			4904 = {
				naval_base = 2
			}
			4965 = {
				naval_base = 3
			}
		}

		set_variable = { vp_value = 0 } 
		set_variable = { highest_vp = 0 } 

		add_core_of = MEX
		
		
	}

	provinces={
		4904 4965 12715 
	}
	
}
