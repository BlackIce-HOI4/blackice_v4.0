
state={
	resources = {
		coal = 4
		iron = 2
	}
	id=496
	name="STATE_496"

	history={
		owner = BRA
		buildings = {
			infrastructure = 2
			industrial_complex = 7
			hydro_power = 2

		}

		set_variable = { vp_value = 0 } 
		set_variable = { highest_vp = 0 } 

		add_core_of = BRA

	}

	provinces={
		2185 2190 2218 4570 5223 7476 7589 8148 8196 8210 8233 10931 10999  12903 12909 12938 12334 8226 
	}
	manpower=1134533
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=small_town
}
