
state={
	resources = {
		coal = 182
		iron = 206
		bauxite = 50
		rubber = 2
		food = 19
	}


	id=51
	name="STATE_51"
	
	manpower = 6144743
	

	local_supplies=0.0
	state_category = metropolis
	
	history={
		owner = GER
		buildings = {
			infrastructure = 8
			industrial_complex = 10
			arms_factory = 6
			air_base = 3
			synthetic_rubber_refinery = 1
			steel_refinery = 4
			power_plant = 6
		}

		set_variable = { vp_value = 26 } 
		set_variable = { highest_vp = 10 } 

		add_core_of = GER
		
		#set_variable = { state_taxes = 614 }

		
		1936.3.7 = {
			set_demilitarized_zone = no
		}
		
		set_demilitarized_zone = yes
		
		1939.1.1 = {
			buildings = {
				air_base = 6
				radar_station = 1
			}
		}
		
		victory_points = { 529 1 }
		victory_points = { 587 5 }
		victory_points = { 3512 1 }
		victory_points = { 3547 1 }
		victory_points = { 6469 10 }
		victory_points = { 6570 1 }
		victory_points = { 9482 1 }
		victory_points = { 9522 1 }
		victory_points = { 3444 5 }
	}

	provinces={
		529 587 3444 3512 3547 6469 6570 9482 9522 
	}
	
}
