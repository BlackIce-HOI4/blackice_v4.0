
state={
	id=511
	name="STATE_511"

	history={
		owner = ARG
		buildings = {
			infrastructure = 2
			industrial_complex = 5
			air_base = 1

		}

		set_variable = { vp_value = 0 } 
		set_variable = { highest_vp = 0 } 

		add_core_of = ARG

	}

	provinces={
		2158 2179 2215 5219 8215 10964 10992 11021 12970 
	}
	manpower=1090274
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=rural
}
