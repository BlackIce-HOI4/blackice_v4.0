
state={


	id=52
	name="STATE_52"
	
	manpower = 2679413
	
	local_supplies=0.0
	state_category = city
	

	history={
		owner = GER
		buildings = {
			infrastructure = 7
			industrial_complex = 5
			arms_factory = 6
			air_base = 6
			air_assembly = 4
			artillery_assembly = 2
			engine_assembly = 4
			aluminium_refinery = 6
		}

		set_variable = { vp_value = 48 } 
		set_variable = { highest_vp = 30 } 

		add_core_of = GER
		
		#set_variable = { state_taxes = 267 }


		1939.1.1 = {
			buildings = {
				air_base = 10
			}
		}
		
		victory_points = { 692 30 }
		victory_points = { 707 8 }
		victory_points = { 708 1 }
		victory_points = { 3705 1 }
		victory_points = { 6540 1 }
		victory_points = { 9652 1 }
		victory_points = { 11620 5 }
		victory_points = { 11638 1 }
	}

	provinces={
		692 707 708 3688 3705 6540 6693 9652 9666 11620 11638 11653 
	}
}
