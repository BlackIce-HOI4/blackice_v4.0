state=
{
	resources = {
		coal = 16
		food = 46
	}


	id=524
	name="STATE_524"
	manpower = 4592537
	buildings_max_level_factor=1
	local_supplies=0.0
	state_category=large_town
	history={
		owner = JAP
		buildings = {
			infrastructure = 4
			industrial_complex = 3
			arms_factory = 2
			air_base = 6
			steel_refinery = 2
			12068 = { #Takao/Pescadores
				naval_base = 6
			}
			7186 = { #Taihoku
				naval_base = 2
			}
		}

		set_variable = { vp_value = 13 } 
		set_variable = { highest_vp = 10 } 

		add_core_of = CHI
		add_core_of = PRC
		#set_variable = { state_taxes = 459 }
		victory_points = {
			7186 10 
		}
		victory_points = {
			12068 3 
		}
	}
	provinces={
		1091 1175 4096 7186 7214 9955 11914 11959 12027 12068
		}
}
