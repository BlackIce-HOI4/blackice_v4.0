
state={
	resources = {
		iron = 46
	}


	id=533
	name="STATE_533" # Tohoku
	manpower = 6575000
	local_supplies=0.0
	state_category = large_city

	history={
		owner = JAP
		buildings = {
			infrastructure = 4
			industrial_complex = 12
			arms_factory = 6
			dockyard = 1
			air_base = 6
			steel_refinery = 2
			aluminium_refinery = 2
			hydro_power = 2
			fuel_silo = 1
		
			9859 = { #Onimato
				naval_base = 4
			}
		}

		set_variable = { vp_value = 20 } 
		set_variable = { highest_vp = 15 } 

		add_core_of = JAP
		#set_variable = { state_taxes = 657 }
		
		victory_points = {
			6994 5
		}
		
		victory_points = {
			7169 15
		}

	}

	provinces={
		1024 1063 1165 3848 4067 4118 4122 4153 6870 6873 6994 7169 9807 9859 9865 11847 12056 
	}
}
