state= {
	resources = {
		oil = 2
	}
	id=537
	name="STATE_537"

	
	manpower = 295196
	local_supplies=0.0
	state_category = rural

	history= {
		owner = JAP
		buildings = {
			infrastructure = 3
			12446 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 1 } 
		set_variable = { highest_vp = 1 } 

		#add_claim_by = SOV # Removed because of too much aggression

		victory_points = { 12446 1 }
	}
	provinces={
		1322 1820 12446
	}
}


