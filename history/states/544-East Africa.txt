
state={
	resources = {
		food = 3
	}
	id=544
	name="STATE_544" #Mozambique
	manpower = 4125000
	
	local_supplies=0.0
	state_category = rural
	
	history={
		owner = POR
		buildings = {
			infrastructure = 3
			industrial_complex = 2
			8245 = {
				naval_base = 1
			}
			2123 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 1 } 
		set_variable = { highest_vp = 1 } 

		#set_variable = { state_taxes = 412 }
		victory_points = {
			8245 1 
		}
		#add_core_of = MZB
	}

	provinces={
		2120 2123 2159 2180 2193 2202 2217 5155 5164 5177 5212 8245 10972 12935 12950 12971 
	}
}
