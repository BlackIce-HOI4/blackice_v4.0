state={
	resources = {
		rubber = 12
	}


	id=548
	name="STATE_548"

	local_supplies=0.0
	state_category = rural

	history={
		owner = ENG
		buildings = {
			infrastructure = 1
		}

		set_variable = { vp_value = 0 } 
		set_variable = { highest_vp = 0 } 

		#set_variable = { state_taxes = 351 }
		#add_core_of = KEN
	}

	provinces={
		3854 7088 7133 8223 12929 12989 
	}
	manpower=3515625
	buildings_max_level_factor=1.000
}
