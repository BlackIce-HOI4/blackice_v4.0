state=
{
	resources = {
		food = 2
	}
	id=550
	name="STATE_550"
	manpower = 600600
	
	local_supplies=0.0
	state_category = rural
	
	history={
		owner = ITA
		buildings = {
			infrastructure = 4
			air_base = 2
			12766 = {
				naval_base = 3
			}
		}

		set_variable = { vp_value = 1 } 
		set_variable = { highest_vp = 1 } 

		victory_points = {
			12766 1
		}
	}
	provinces={
		5017 5047 5091 8043 12723 12766
	}
}
