
state={
	resources = {
		food = 18
	}


	id=566
	name="STATE_566"
	
	manpower = 755237
	
	local_supplies=0.0
	state_category = small_town	#6 slots
	

	history = {
		owner = SOV
		buildings = {
			infrastructure = 4
			industrial_complex = 2
			arms_factory = 4
			air_base = 1
			steel_refinery = 2
			#slots
		}

		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 3 } 

		add_core_of = SOV
		
		
		victory_points = { 4797 3 }
		victory_points = { 12525 1 }
		victory_points = { 10524 1 }
	}

	provinces = {
		1707 1727 1762 1810 1829 4797 4815 7838 10524 10543 10596 10661 12525 12540 12647 13152 
	}
	
	buildings_max_level_factor=1.000
}
