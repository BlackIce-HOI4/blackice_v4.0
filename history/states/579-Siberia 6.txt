
state={
	id=579
	name="STATE_579" # Salekhard
	manpower = 45484
	
	local_supplies=0.0
	state_category = wasteland

	history={
		owner = SOV
		buildings = {
			infrastructure = 1
		}

		set_variable = { vp_value = 1 } 
		set_variable = { highest_vp = 1 } 

		victory_points = { 4669 1 }
		add_core_of = SOV
	}

	provinces={
		1664 1684 3169 4669 7689 7738 8461 9142 
	}
}
