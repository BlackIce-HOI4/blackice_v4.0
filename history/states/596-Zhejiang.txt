state = {
	resources = {
		tungsten = 4
		food = 4
	}


	id=596
	name="STATE_596"
	local_supplies=0.0
	state_category=city
	manpower = 21230700
	history = {
		owner = CHI
		buildings = {
			infrastructure = 4
			industrial_complex = 2
			arms_factory = 2
			air_assembly = 2
			steel_refinery = 2
			7191 = {
				naval_base = 1
				coastal_bunker = 1
				bunker = 1
			}
		}

		set_variable = { vp_value = 15 } 
		set_variable = { highest_vp = 15 } 

		add_core_of = CHI
		add_core_of = PRC

		#set_variable = { state_taxes = 2123 }
		victory_points = { 
			7191 15 
		}
		1938.10.25 = {
			JAP = {
				set_province_controller = 4042
				set_province_controller = 7191
				set_province_controller = 10014
				set_province_controller = 10101
			} 
		}
	}
	provinces = {
		4042 7191 10014 10101
	}
	buildings_max_level_factor=1.500
}
