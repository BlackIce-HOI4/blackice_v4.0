
state={
	resources = {
		iron = 12
		food = 36
	}
	id=598
	name="STATE_598"
	local_supplies=0.0
	state_category=town
	manpower=23669300
	history={
		owner = CHI
		buildings = {
			infrastructure = 3
		}

		set_variable = { vp_value = 8 } 
		set_variable = { highest_vp = 5 } 

		add_core_of = CHI
		add_core_of = PRC

		#set_variable = { state_taxes = 2366 }
		1938.10.25 = {
			controller = JAP
		}
		victory_points = { 4005 5 }
		victory_points = {
			12067 3
		}
	}
	provinces={
		4001 4123 4005 4031 4082 4148 7142 7181 9953 10095 12067 
	}
	buildings_max_level_factor=1.000
}
