
state={
	resources = {
		tungsten = 4
		food = 6
	}


	id=605
	name="STATE_605"
	manpower = 52963300
	local_supplies=0.0
	state_category=city	#14 slots
	history={
		owner = SCC
		buildings = {
			infrastructure = 3
			industrial_complex = 5
			arms_factory = 2
			air_base = 2
			steel_refinery = 2
			#slots
		}

		set_variable = { vp_value = 15 } 
		set_variable = { highest_vp = 10 } 

		add_core_of = SCC
		add_core_of = CHI
		add_core_of = PRC
		#set_variable = { state_taxes = 529 }
		1939.1.1 = {
			buildings = {
				industrial_complex = 3
				infrastructure = 6
				air_base = 2
				arms_factory = 1
			}
		}
		victory_points = { 4925 10 }
		victory_points = {
			2045 5 
		}
	}
	provinces={
		1367 2045 4375 4925 10132 
	}
	buildings_max_level_factor=1.000
}
