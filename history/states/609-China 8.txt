
state={
	resources = {
		coal = 6
		food = 3
	}


	id=609
	name="STATE_609" # East Hebei
	manpower=6500000
	local_supplies=0.0
	state_category=large_town
	buildings_max_level_factor=1.500
	history={
		owner = EHA
		buildings = {
			infrastructure = 5
			industrial_complex = 2
			arms_factory = 2
			3900 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 7 } 
		set_variable = { highest_vp = 3 } 

		#owner = JAP
		#set_variable = { state_taxes = 650 }
		add_core_of = EHA
		add_core_of = CHI
		add_core_of = PRC
		victory_points = {
			900 2 
		}
		victory_points = {
			3900 3 
		}
		victory_points = {
			11822 2
		}
	}
	provinces={
		900 1052 3830 3900 3955 6969 11822 12043 
	}
}
