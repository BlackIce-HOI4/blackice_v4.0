state=
{
	id=648
	name="STATE_648"
	local_supplies=0.0
	state_category = tiny_island
	manpower=42
	history={
		owner = JAP
		buildings = {
			infrastructure = 3
			air_base = 1
			13026 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 0 } 
		set_variable = { highest_vp = 0 } 

		set_state_flag = JAP_controls
		add_core_of = JAP
	}
	provinces= { 13026 }
}
