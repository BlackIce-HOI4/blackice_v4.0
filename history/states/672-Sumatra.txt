
state={
	resources = {
		coal = 6
		bauxite = 170
		oil = 60
		rubber = 248
		food = 26
	}


	id=672
	name="STATE_672"
	
	manpower=8254900
	
	local_supplies=0.0
	state_category=large_town
	

	history={
		owner = INS
		buildings = {
			infrastructure = 3
			industrial_complex = 5
			arms_factory = 2
			air_base = 1
			12138 = {
				naval_base = 2
			}
			12268 = {
				naval_base = 3
			}
			4361 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 4 } 
		set_variable = { highest_vp = 4 } 

		#set_variable = { state_taxes = 825 }
		add_core_of = INS
		
		
		victory_points = {
			12268 4
		}
	}

	provinces={
		1246 1274 1277 1313 1332 1342 1345 1373 1401 1428 1533 4211 4276 4320 4361 4364 4381 7308 7368 7384 7396 7440 10150 10194 10209 10225 10294 10322 10354 10382 12126 12138 12165 12168 12181 12184 12209 12212 12268 12296 12343 12415 
	}
	
	buildings_max_level_factor=1.000
}
