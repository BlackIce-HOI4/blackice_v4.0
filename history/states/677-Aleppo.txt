state={
	resources = {
		food = 1
	}
	id=677
	name="STATE_677"


	manpower = 1325594
	

	local_supplies=0.0
	state_category = town
	
	history={
		owner = SYR
		buildings = {
			infrastructure = 3
			industrial_complex = 5
		}

		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 3 } 

		victory_points = {
			12473 3
		}
		victory_points = { 1056 1 }
		victory_points = { 1088 1 }
		add_core_of = SYR

	}

	
	provinces={
		1056 1072 1088 7140 7671 10087  12473 
	}
}
