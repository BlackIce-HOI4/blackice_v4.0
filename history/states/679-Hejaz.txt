state={
	id=679
	name="STATE_679"
	manpower = 1511000
	
	local_supplies=0.0
	state_category = pastoral

	history={
		owner = SAU
		buildings = {
			infrastructure = 3
			12883 = {
				naval_base = 5
			}
		}

		set_variable = { vp_value = 4 } 
		set_variable = { highest_vp = 3 } 

		victory_points = {
			12758 3 #Medina
		}
		victory_points = {
			12883 1 
		}
		add_core_of = SAU
	}

	provinces={
		1033 1923 2027 2033 2090 2095 4981 5006 5037 7934 8073 8092 10748 10786 10835 12101 12758 12883 
	}
}
