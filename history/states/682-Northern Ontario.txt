
state={


	id=682
	name="STATE_682"

	history={
		owner = CAN
		buildings = {
			infrastructure = 5
			industrial_complex = 5
			hydro_power = 2

		}

		set_variable = { vp_value = 0 } 
		set_variable = { highest_vp = 0 } 

		add_core_of = CAN

	}

	provinces={
		602 761 763 777 1299 1769 1782 3736 4753 4775 4784 4869 4875 6510 7366 7794 7899 9467 9480 9740 10621 10718 11571 12278 12582 12701 13094 13161 
	}
	manpower=309645
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=rural
}
