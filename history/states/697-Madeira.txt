
state={
	id=697
	name="STATE_697"
	
	manpower = 211600
	
	local_supplies=0.0
	state_category = small_island

	history={
		owner = POR
		buildings = {
			infrastructure = 3
			air_base = 1
			3118 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 1 } 
		set_variable = { highest_vp = 1 } 

		add_core_of = POR
		
		
		victory_points = { 3118 1 }
	}
	
	provinces={
		3118 
	}
}
