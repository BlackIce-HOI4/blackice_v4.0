state={
	id=703
	name="STATE_703"
	manpower=458
	local_supplies=0.0
	state_category = tiny_island
	history= {
		owner = ENG
		buildings = {
			infrastructure = 1
			13013 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 0 } 
		set_variable = { highest_vp = 0 } 

	}
	
	provinces={
		13013 
	}
}
