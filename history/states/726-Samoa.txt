state={
	id=726
	name="STATE_726"
	manpower=123214
	local_supplies=0.0
	state_category = small_island

	history={
		owner = NZL
		buildings = {
			infrastructure = 2
			7290 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 0 } 
		set_variable = { highest_vp = 0 } 

	}
	provinces={
		7290 13080 
	}
}
