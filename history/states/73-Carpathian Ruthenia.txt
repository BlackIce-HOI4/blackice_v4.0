
state = {
	resources = {
		coal = 10
	}
	id = 73
	name = "STATE_73"
	
	manpower = 754300
	
	local_supplies=0.0
	state_category = rural
	
	history = {
		owner = CZE
		buildings = {
			infrastructure = 5
			air_base = 1
		}

		set_variable = { vp_value = 1 } 
		set_variable = { highest_vp = 1 } 

		add_core_of = CZE
		add_core_of = HUN
		add_core_of = UKR
		

		1939.3.14 = {
			owner = HUN
			controller = HUN
			remove_claim_by = HUN
			add_core_of = HUN
			add_core_of = SLO
		}
		
		victory_points = { 3548 1 }
	}

	provinces={ #11691 #re-drawn and given to southern slovakia because hungary got it as part of that
		3548 6571 9563 11536 
	}
}
