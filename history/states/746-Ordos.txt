
state={
	resources = {
		food = 24
	}
	id=746
	name="STATE_746"
	history={
		owner = SHX
		buildings = {
			infrastructure = 1
			industrial_complex = 2
		}

		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 5 } 

		add_core_of = SHX
		add_core_of = CHI
		add_core_of = PRC
		victory_points = {
			3427 5 
		}
	}
	provinces={
		1959 3427 4960 4986 10451 10854 12706 
	}
	manpower=500000
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=pastoral
}
