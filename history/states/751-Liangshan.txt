
state={
	resources = {
		coal = 14
	}
	id=751
	name="STATE_751"

	history={
		owner = JAP
		buildings = {
			infrastructure = 3

		}

		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 5 } 

		add_core_of = KOR
		victory_points = {
			1148 5 
		}

	}

	provinces={
		1148 4004 7021 9936 9981 11958 12011 
	}
	manpower=300000
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=pastoral
}
