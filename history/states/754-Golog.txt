
state={
	resources = {
		food = 5
	}
	id=754
	name="STATE_754"
	history={
		owner = SIK
		buildings = {
			infrastructure = 1
			industrial_complex = 2
			arms_factory = 2
		}

		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 5 } 

		add_core_of = SIK
		add_core_of = CHI
		add_core_of = PRC
		victory_points = { 4828 5 }
	}
	provinces={
		1703 4682 4828 7949 12119 
	}
	manpower=400000
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=town
}

