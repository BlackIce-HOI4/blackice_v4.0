
state={
	resources = {
		food = 11
	}
	id=759
	name="STATE_759"
	history={
		owner = SIK
		buildings = {
			infrastructure = 1
		}

		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 5 } 

		add_core_of = SIK
		add_core_of = CHI
		add_core_of = PRC
		victory_points = {
			7702 5 
		}
	}
	provinces={
		4770 7702 7792 10545 
	}
	manpower=70000
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=town
}
