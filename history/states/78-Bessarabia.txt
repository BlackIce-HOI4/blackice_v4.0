
state={
	resources = {
		food = 18
	}
	id=78
	name="STATE_78"

	history={
		owner = ROM
		buildings = {
			infrastructure = 4
			arms_factory = 6
			air_base = 2
			#industrial_complex = 1

		}

		set_variable = { vp_value = 11 } 
		set_variable = { highest_vp = 10 } 

		add_core_of = ROM
		victory_points = { 11686 10 }
		victory_points = { 3577 1 }

		#set_variable = { state_taxes = 286 }

	}

	provinces={
		414 565 3577 3707 3724 6600 9683 11686 11705 
	}
	manpower=2863400
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=town
}
