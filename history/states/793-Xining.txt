
state={
	id=793
	name="STATE_793"
	history={
		owner = XSM
		buildings = {
			infrastructure = 1
			industrial_complex = 5
			arms_factory = 2
		}

		set_variable = { vp_value = 10 } 
		set_variable = { highest_vp = 10 } 

		add_core_of = XSM
		add_core_of = CHI
		add_core_of = PRC
		1938.10.25 = {
			owner = CHI
		add_core_of = CHI
		add_core_of = PRC

		}
		victory_points = {
			12732 10 
		}
	}
	provinces={
		4888 7270 10490 12732 
	}
	manpower=1000000
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=town
}
