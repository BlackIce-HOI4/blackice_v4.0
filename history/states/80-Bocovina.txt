
state={
	resources = {
		food = 18
	}
	id=80
	name="STATE_80"

	history={
		owner = ROM
		buildings = {
			infrastructure = 6
			arms_factory = 4
			air_base = 1

		}

		set_variable = { vp_value = 10 } 
		set_variable = { highest_vp = 10 } 

		add_core_of = ROM
		add_core_of = UKR
		victory_points = {
			9548 10 
		}

	}

	provinces={
		577 3407 9548 
	}
	manpower=474600
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=rural
}
