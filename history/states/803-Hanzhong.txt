
state={
	resources = {
		food = 7
	}
	id=803
	name="STATE_803"
	history={
		owner = ZXL
		buildings = {
			infrastructure = 1
			industrial_complex = 3
			arms_factory = 4
		}

		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 5 } 

		add_core_of = ZXL
		add_core_of = CHI
		add_core_of = PRC
		set_variable = {
			state_taxes = 300
		}
		1939.1.1 = {
			buildings = {
				infrastructure = 6
			}
		}
		victory_points = {
			2091 5 
		}
	}
	provinces={
		2091 4167 4256 4295 7256 7406 10144 10257 12408 
	}
	manpower=3000000
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=small_town
}
