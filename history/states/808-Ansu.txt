state = {
	resources = {
		tungsten = 8
	}


	id=808
	name="STATE_808"
	manpower=1000000
	local_supplies=0.0
	state_category=town
	history = {
		owner = XSM
		buildings = {
			infrastructure = 3
		}

		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 5 } 

		add_core_of = XSM
		add_core_of = CHI
		add_core_of = PRC
		victory_points = {
			5076 5#Jiuquan
		}
	}
	provinces = {
		1778 5076 7971 12596 12820 
	}
	buildings_max_level_factor=1.000
}
