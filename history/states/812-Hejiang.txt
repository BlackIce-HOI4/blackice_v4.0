
state={
	id=812
	name="STATE_812"
	history={
		owner = MAN
		buildings = {
			infrastructure = 3
			industrial_complex = 2
			arms_factory = 2
			air_base = 5
		}

		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 5 } 

		add_core_of = MAN
		add_core_of = CHI
		add_core_of = PRC
		victory_points = {
			7670 5 
		}
		
		force_enable_resistance = yes 
		start_resistance = yes
	}
	provinces={
		1571 3815 4551 5427 6811 6863 7569 7670 9623 9798 10215 10463 10519 10521 11906 12342 12355 12443 12482 12500 
	}
	manpower=500000
	buildings_max_level_factor=1.500
	local_supplies=0.0
	state_category=town
}
