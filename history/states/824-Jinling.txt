state={
	resources = {
		tungsten = 12
		food = 20
	}


	id=824
	name="STATE_988"
	local_supplies=0.0
	state_category=city
	manpower=4000000
	history={
		owner = CHI
		buildings = {
			infrastructure = 5
			industrial_complex = 2
			arms_factory = 4
			air_base = 1
			power_plant = 2
		}

		set_variable = { vp_value = 20 } 
		set_variable = { highest_vp = 20 } 

		add_core_of = CHI
		add_core_of = PRC

		#set_variable = { state_taxes = 400 }
		victory_points = {
			11913 20 
		}
		1938.10.25 = {
			controller = JAP
		}
	}
	provinces={
		996 11913 11982 
	}
	buildings_max_level_factor=1.000
}
