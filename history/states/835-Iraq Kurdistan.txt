state={
	id=835
	name="STATE_977"
	
	manpower=591370
	
	local_supplies=0.0
	state_category=pastoral
	
	history={
		owner = IRQ
		buildings = {
			infrastructure = 1
		}

		set_variable = { vp_value = 1 } 
		set_variable = { highest_vp = 1 } 

		add_core_of = IRQ
		victory_points = { 5014 1 }
	}
	
	provinces={
		5014 6826 10793 10811 
	}
	
	buildings_max_level_factor=1.000
}
