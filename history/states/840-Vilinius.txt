
state = {
	id = 840
	name = "STATE_840"
	
	manpower = 1100000
	
	local_supplies=0.0
	state_category = city
	
	history = {
		owner = POL
		buildings = {
			infrastructure = 5
			industrial_complex = 8
			air_base = 5
		}

		set_variable = { vp_value = 16 } 
		set_variable = { highest_vp = 15 } 

		add_core_of = POL
		add_core_of = LIT
		
		
		
		victory_points = { 9274 1 }
		victory_points = { 3320 15 }
	}
	
	provinces = {
		3320 9274 9295 11342 11391 
	}
	
	buildings_max_level_factor=1.000
}
