
state={
	id=843
	name="STATE_843"

	history={
		owner = CZE
		buildings = {
			infrastructure = 8
			industrial_complex = 5
			arms_factory = 18
			anti_air_building = 5
			tank_assembly = 4
			artillery_assembly = 2
			engine_assembly = 2
			power_plant = 2
		
		}

		set_variable = { vp_value = 30 } 
		set_variable = { highest_vp = 30 } 

		add_core_of = CZE
		victory_points = {
			11542 30 
		}

	}

	provinces={
		11542 
	}
	manpower=718300
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=metropolis
}
