state={
	resources = {
		food = 21
	}
	id=858
	name="STATE_966"
	
	manpower=2486593
	
	local_supplies=0.0
	state_category=city
	
	history={
		owner = ITA
		buildings = {
			infrastructure = 5
			industrial_complex = 5
			dockyard = 3
			air_base = 2
			subyard = 1
			fuel_silo = 1
			11837 = {
				naval_base = 10
			}
			9784 = {
				naval_base = 2
			}
			11998 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 17 } 
		set_variable = { highest_vp = 10 } 

		add_core_of = ITA
		
		
		1939.1.1 = {
			buildings = {
				dockyard = 4
			}
		}
		
		victory_points = { 6979 1 }
		victory_points = { 11837 5 }
		victory_points = { 9784 10 }
		victory_points = { 883 1 }
	}
	
	provinces={
		6843 6979 9784 11837 11998 6939 6980 883 3966
	}
	
	buildings_max_level_factor=1.000
}
