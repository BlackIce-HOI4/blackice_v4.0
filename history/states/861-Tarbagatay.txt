state={
	id=861
	name="STATE_861"
	history={
		owner = SIK
		buildings = {
			infrastructure = 3
			#industrial_complex = 1
		}

		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 5 } 

		add_core_of = SIK
		add_core_of = CHI
		add_core_of = PRC
		victory_points = {
			1708 5 
		}
	}
	provinces={
		1708 1783 1844 
	}
	manpower=300000
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=town
}
