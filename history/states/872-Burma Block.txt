
state={
	resources = {
		coal = 24
		food = 34
	}
	id=872
	name="STATE_991"

	history={
		owner = RAJ
		buildings = {
			infrastructure = 2

		}

		set_variable = { vp_value = 1 } 
		set_variable = { highest_vp = 1 } 

		add_core_of = BUR
		add_claim_by = RAJ
		
		victory_points = {
			4425 1 
		}

	}

	provinces={
		1082 1197 1934 4087 4318 4425 4588 4996 8063 10386 10486 10920 12335 12464 12492 
	}
	manpower=1213723
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=pastoral
}
