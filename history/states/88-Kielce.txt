
state={
	resources = {
		coal = 34
		oil = 6
	}


	id=88
	name="STATE_88"
	
	manpower=4392800
	
	local_supplies=0.0
	state_category=large_city
	

	history={
		owner = POL
		buildings = {
			infrastructure = 6
			industrial_complex = 5
			arms_factory = 4
			air_base = 4
			artillery_assembly = 2
			tank_assembly = 1
			hydro_power = 2
		}

		set_variable = { vp_value = 18 } 
		set_variable = { highest_vp = 15 } 

		add_core_of = POL

		#set_variable = { state_taxes = 439 }
		
		victory_points = { 442 1 }
		victory_points = { 3410 1 }
		victory_points = { 6522 1 }
		victory_points = { 9427 15 }
	}

	provinces={
		417 442 466 3410 3434 6499 6522 9412 9427 11398 11413 11507 13255 
	}
	
	buildings_max_level_factor=1.000
}
