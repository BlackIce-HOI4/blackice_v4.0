
state = {
	id = 880
	name = "STATE_880"
	
	history = {
		owner = RAJ
		buildings = {
			infrastructure = 1
		}

		set_variable = { vp_value = 0 } 
		set_variable = { highest_vp = 0 } 

		add_core_of = BUR
		add_claim_by = RAJ
		#add_core_of = ZIM		
	}

	provinces = {
		 4175 7400 7647 7909 7974 12317
	}
	manpower = 788920
	buildings_max_level_factor = 1.000
	local_supplies=0.0
	state_category = rural
}
