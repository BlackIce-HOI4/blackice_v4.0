state={
	id=889
	name="STATE_889"

	manpower=4289878

	local_supplies=0.0
	state_category=metropolis

	history={
		owner = USA
		buildings = {
			infrastructure = 7
			industrial_complex = 10
			arms_factory = 4
			dockyard = 3
			shipyard_capital_building = 1
			air_assembly = 2
			shipyard = 1
			subyard = 1
			6732 = {
				naval_base = 8
			}
		}

		set_variable = { vp_value = 20 } 
		set_variable = { highest_vp = 20 } 

		add_core_of = USA
		#set_variable = { state_taxes = 428 }

		victory_points = {
			6732 20
		}
	}

	provinces={
		715 718 905 908 3710 3906 6732
	}


	buildings_max_level_factor=1.000

}
