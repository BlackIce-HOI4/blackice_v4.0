
state={
	resources = {
		food = 6
	}
	id=915
	name="STATE_915"
	
	manpower=42967
	
	local_supplies=0.0
	state_category=town
	
	history={
		owner = YUG
		buildings = {
			infrastructure = 5
			6889 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 5 } 

		add_core_of = YUG
		add_core_of = CRO
		
		
		victory_points = { 6889 5 }
	}
	
	provinces={
		3868 6889 11816 
	}
	
	buildings_max_level_factor=1.000
}
