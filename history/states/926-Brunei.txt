state={
	resources = {
		oil = 20
	}


	id=926
	name="STATE_926"
	
	manpower=23500
	
	local_supplies=0.0
	state_category=large_town
	

	history={
		owner = MAL
		buildings = {
			infrastructure = 5
			industrial_complex = 2
			arms_factory = 2
			air_base = 1
			7387 = {
				naval_base = 1
			}
		}

		set_variable = { vp_value = 2 } 
		set_variable = { highest_vp = 2 } 

		add_core_of = MAL
		#add_core_of = BRU
		
		
		victory_points = {
			7387 2 
		}
	}
	
	provinces={
		7387 
	}
	
	buildings_max_level_factor=1.000
}
