
state={
	id=946
	name="STATE_1004"

	history={
		owner = EGY
		buildings = {
			infrastructure = 3
			air_base = 1
			7079 = {
				bunker = 2
			}
			9989 = {
				naval_base = 1
			}

		}
		
		set_variable = { vp_value = 5 } 
		set_variable = { highest_vp = 2 } 

		add_core_of = EGY
		victory_points = {
			9989 2 
		}
		victory_points = {
			7079 2 
		}
		victory_points = {
			7079 1 
		}

	}

	provinces={
		5078 7079 9989 10907 11967 12033 12091 12816 13266 
	}
	manpower=31534
	buildings_max_level_factor=1.000
	local_supplies=0.0
	state_category=wasteland
}
