﻿##############################
##### ENGLAND PLAYER OOB #####
##############################
instant_effect = {
	set_technology = {
		################ airunits ##############
		tech_raf = 1
		tech_english_aircraft1 = 1
		tech_hawker_demon_equipment_1 = 1 
		tech_armstrong_scimitar_equipment_1 = 1
		tech_gauntlet_fighter_equipment_1 = 1
		tech_hawker_hind_equipment_1 = 1 
		tech_vickers_valentia_equipment_1 = 1 
		tech_hawker_osprey_fighter_equipment_2 = 1
		tech_hendon_equipment_1 = 1 
		tech_supermarine_scapa_equipment_1 = 1
		tech_saro_london_equipment_1 = 1
		tech_vickers_valentia_transport_equipment_1 = 1
	}
	# Air Recon for LaR only
	if = {
		limit = { has_dlc = "La Resistance" }
		set_technology = {
			tech_hawker_audax_scout_equipment_1 = 1
			tech_hawker_hector_scout_equipment_1 = 1
		}
	}
}

### Army OOB ###

units = {
	##Aldershot Command#############################
	division= {
		#1st Army Tank Regiment
		division_name = {
			is_name_ordered = yes
			name_order = 50
		}
		location = 9239
		division_template = "Army Tank Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.75
		force_equipment_variants = { 
			trm_light_tank_chassis_eng_vickers_light_1 = { owner = "ENG" version_name = "Light Mk.II" }
			trm_infantry_tank_chassis_eng_vickers_medium_1 = { owner = "ENG" version_name = "Medium Mk.I" }
		}
	}
	division= {
		#1st Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 9239
		division_template = "Infantry Division"
		start_experience_factor = 0.5
	}
	division= {
		#2nd Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 9239
		division_template = "Infantry Division"
		start_experience_factor = 0.5
	}
	
	##East Anglia Area###############################
	division= {
		#54th 'East Anglia' Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 54
		}
		location = 11253
		division_template = "Territorial Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	
	##Home Counties Area################################
	#division= {
	#	name = "12th 'Eastern' Division"
	#	location = 11221
	#	division_template = "Territorial Division"
	#	start_experience_factor = 0.3
	#	start_equipment_factor = 0.1		# should be event activated ?
	#}
	
	##London District####################################
	division= {
		#The London Division
		division_name = {
			is_name_ordered = yes
			name_order = 200
		}
		location = 6103
		division_template = "Motorised Brigade"
		start_experience_factor = 0.4
	}
	division= {
		#Guards Infantry Brigade
		division_name = {
			is_name_ordered = yes
			name_order = 201
		}
		location = 6103
		division_template = "Motorised Brigade"
		start_experience_factor = 0.6
	}
	
	##Northern Command#################################
	division= {
		#5th Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 351
		division_template = "Infantry Division"
		start_experience_factor = 0.4
	}
	
	##Northumbrian Area#################################
	division= {
		#50th 'Northumbrian' Division
		division_name = {
			is_name_ordered = yes
			name_order = 50
		}
		location = 9397
		division_template = "Motorised Division - 1936"
		start_experience_factor = 0.4
		start_equipment_factor = 0.3
	}
	
	##West Riding Area##################################
	#division= {
	#	name = "46th 'North Midlands' Division"
	#	location = 11297
	#	division_template = "Territorial Division"
	#	start_experience_factor = 0.2
	#	start_equipment_factor = 0.1		# should be event activated ?
	#}
	division= {
		#49th 'West Riding' Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 49
		}
		location = 9379
		division_template = "Territorial Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	division= {
		#5th Cavalry Brigade
		division_name = {
			is_name_ordered = yes
			name_order = 30
		}
		location = 9379
		division_template = "Cavalry Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
		force_equipment_variants = { 
			trm_light_tank_chassis_eng_vickers_light_1 = { owner = "ENG" version_name = "Light Mk.III" }
		}
	}
	
	##Scottish Command#################################
	
	#Highland Area####################################
	
	##Lowland Area#####################################
	division= {
		#52nd 'Lowland' Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 52
		}
		location = 9392
		division_template = "Infantry Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.3
	}
	division= {
		#51st 'Highland' Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 51
		}
		location = 9392
		division_template = "Infantry Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.3
	}
	
	##Eastern Command###############################
	division = {
		#4th Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 364
		division_template = "Infantry Division"
		start_experience_factor = 0.4
	}
	division = {
		#44th 'Home Counties' Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 44
		}
		location = 364
		division_template = "Territorial Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	
	##Southern Command#####################################
	division= {
		#3rd Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 9458
		division_template = "Infantry Division"
		start_experience_factor = 0.4
	}
	
	##South-Western Area#################################
	division= {
		#43rd 'Wessex' Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 43
		}
		location = 540
		division_template = "Territorial Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.3
	}
	
	##South Midland Area###############################
	division= {
		#43rd 'Wessex' Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 48
		}
		location = 540
		division_template = "Territorial Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.3
	}
	
	##Western Command######################################
	
	##Welsh Area###########################################
	division= {
		#43rd 'Wessex' Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 53
		}
		location = 540
		division_template = "Territorial Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.3
	}
	division= {
		#Belfast Garrison
		division_name = {
			is_name_ordered = yes
			name_order = 211
		}
		location = 3379
		division_template = "Regular Garrison"
		start_experience_factor = 0.3
	}
	
	##West Lancashire Area###############################
	division= {
		#55th 'West Lancashire' Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 55
		}
		location = 6384
		division_template = "Motorised Division - 1936"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	division= {
		#6th Cavalry Brigade
		division_name = {
			is_name_ordered = yes
			name_order = 31
		}
		location = 6384
		division_template = "Cavalry Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
		force_equipment_variants = { 
			trm_light_tank_chassis_eng_vickers_light_1 = { owner = "ENG" version_name = "Light Mk.III" }
		}
	}
	
	##East Lancashire Area#########################
	division= {
		#42nd 'East Lancashire' Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 42
		}
		location = 540
		division_template = "Territorial Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.3
	}
	
	##Gibraltar#####################################
	division= {
		#British Troops on Gibraltar
		division_name = {
			is_name_ordered = yes
			name_order = 212
		}
		location = 4135
		division_template = "Fortress Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division= {
		#Gibraltar Defence Force
		division_name = {
			is_name_ordered = yes
			name_order = 213
		}
		location = 4135
		division_template = "Colonial Volunteer Fortress Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	
	##Malta############################################
	division= {
		#Malta Fortress
		division_name = {
			is_name_ordered = yes
			name_order = 214
		}
		location = 12003
		division_template = "Fortress Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division= {
		#Malta Brigade
		division_name = {
			is_name_ordered = yes
			name_order = 215
		}
		location = 12003
		division_template = "Colonial Volunteer Fortress Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	
	### Middle East Command ########################
	
	##Western Desert Command#########################
	#division= {
	#	name = "7th Infantry Div. (Light"
	#	location = 7011
	#	division_template = "Infantry Division (ME)"
	#	start_experience_factor = 0.1
	#	start_equipment_factor = 0.1		# should be event activated ?
	#}
	#division= {
	#	name = "8th Infantry Division"
	#	location = 4076
	#	division_template = "Territorial Division"
	#	start_experience_factor = 0.1
	#	start_equipment_factor = 0.1		# should be event activated ?
	#}
	#division= {
	#	name = "11th Indian Brigade Group"
	#	location = 7011
	#	division_template = "Infantry Division (ME)"
	#	start_experience_factor = 0.3
	#	start_equipment_factor = 0.5 		# should be event activated
	#
	#}
	
	##Egypt Command#####################################
	division= {
		#Cairo Brigade"
		division_name = {
			is_name_ordered = yes
			name_order = 216
		}
		location = 7011
		division_template = "Regular Garrison"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	division= {
		#Cavalry Brigade (Egypt)
		division_name = {
			is_name_ordered = yes
			name_order = 32
		}
		location = 7011
		division_template = "Cavalry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
		force_equipment_variants = { 
			trm_light_tank_chassis_eng_vickers_light_2 = { owner = "ENG" version_name = "Light Mk.IV" }
		}
	}
	division= {
		#The Canal Brigade
		division_name = {
			is_name_ordered = yes
			name_order = 217
		}
		location = 1155
		division_template = "Regular Garrison"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	division= {
		#Cyprus Garrison
		division_name = {
			is_name_ordered = yes
			name_order = 31
		}
		location = 11984
		division_template = "Local Defence Force"
		start_experience_factor = 0.1
		start_equipment_factor = 0.15
	}
	#division= {
	#	name = "Badiyat Ash Sham Garrison"
	#	location = 1155 #suez canal east bank, east of port said
	#	division_template = "Local Defence Force"
	#	start_experience_factor = 0.1
	#	start_equipment_factor = 0.15
	#}
	
	##Palestine Command##############################
	#division= {	 # the Jerusalem & Lydda Areas division has been moved to BMP_1936
	#	#Jerusalem & Lydda Areas
	#	division_name = {
	#		is_name_ordered = yes
	#		name_order = 219
	#	}
	#	location = 1086 #jerusalem in Palestine, was 4206 Tel Aviv
	#	division_template = "Regular Garrison"
	#	force_equipment_variants = { infantry_equipment_0 = { owner = "ENG" } }
	#	start_experience_factor = 0.1
	#	start_equipment_factor = 0.8
	#}
	division= {
		#Transjordan Area
		division_name = {
			is_name_ordered = yes
			name_order = 32
		}
		location = 7151
		division_template = "Local Defence Force"
		force_equipment_variants = { infantry_equipment_0 = { owner = "ENG" } }
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
	#division= {
	#	name = "Gaza Garrison"
	#	location = 4088
	#	division_template = "Local Defence Force"
	#	force_equipment_variants = { infantry_equipment_0 = { owner = "ENG" } }
	#	start_experience_factor = 0.1
	#	start_equipment_factor = 0.1
	#
	#}
	division= {	  
		#British Troops in Aden
		division_name = {
			is_name_ordered = yes
			name_order = 34
		}
		location = 5074
		division_template = "Local Defence Force"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	
	##East Africa Command#######################################
	division= {
		#British Troops in the Sudan
		division_name = {
			is_name_ordered = yes
			name_order = 222
		}
		location = 12806
		division_template = "Local Defence Force"
		force_equipment_variants = { infantry_equipment_0 = { owner = "ENG" } }
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division= {
		#Sudan Defence Force
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 2046
		division_template = "Local Defence Force"
		force_equipment_variants = { infantry_equipment_0 = { owner = "ENG" } }
		start_experience_factor = 0.3
		start_equipment_factor = 0.4
	}
	division= {
		#Somaliland Camel Corps
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 12759
		division_template = "Local Defence Force"
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division= {
		#Rhodesia Groups
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 10929
		division_template = "Local Defence Force"			
		force_equipment_variants = { infantry_equipment_0 = { owner = "ENG" } }
		start_equipment_factor = 0.3
	}
	division= {
		#Northern Brigade (King's African Rifles)
		division_name = {
			is_name_ordered = yes
			name_order = 581
		}
		location = 5210
		division_template = "Local Defence Force"
		force_equipment_variants = { infantry_equipment_0 = { owner = "ENG" } }
		start_equipment_factor = 0.15
	}
	division= {
		#Southern Brigade (King's African Rifles)
		division_name = {
			is_name_ordered = yes
			name_order = 582
		}
		location = 2196
		division_template = "Local Defence Force"
		force_equipment_variants = { infantry_equipment_0 = { owner = "ENG" } }
		start_equipment_factor = 0.15
	}
	division= {
		#Seychelles Garrison
		division_name = {
			is_name_ordered = yes
			name_order = 226
		}
		location = 2188
		division_template = "Small Colonial Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	division= {
		#Mauritius Territorial Force
		division_name = {
			is_name_ordered = yes
			name_order = 227
		}
		location = 13018
		division_template = "Small Colonial Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	division= {
		#Christmas Island Garrison
		division_name = {
			is_name_ordered = yes
			name_order = 228
		}
		location = 13005
		division_template = "Small Colonial Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	
	##Atlantic Command####################################
	division= {
		#2 Royal West African Frontier Force
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 10862
		division_template = "Local Defence Force"
		start_experience_factor = 0.3
		start_equipment_factor = 0.4
	}
	division= {
		#1 Royal West African Frontier Force
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 2050
		division_template = "Local Defence Force"
		start_experience_factor = 0.3
		start_equipment_factor = 0.4
	}
	division= {
		#3 Royal West African Frontier Force
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 2038
		division_template = "Local Defence Force"
		start_experience_factor = 0.3
		start_equipment_factor = 0.4
	}
	division= {
		#4 Royal West African Frontier Force
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 4989
		division_template = "Small Colonial Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.4
	}
	#division= {
	#	name = "Nigeria Garrison"
	#	location = 2050
	#	division_template = "Small Colonial Garrison"
	#	start_experience_factor = 0.3
	#	start_equipment_factor = 0.3
	#}
	#division= {
	#	name = "Gambia Garrison"
	#	location = 4989
	#	division_template = "Small Colonial Garrison"
	#	start_experience_factor = 0.3
	#	start_equipment_factor = 0.3
	#}
	division= {
		#Bermuda Garrison
		division_name = {
			is_name_ordered = yes
			name_order = 229
		}
		location = 13010
		division_template = "Colonial Volunteer Fortress Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division= {
		#British Troops on Jamaica
		division_name = {
			is_name_ordered = yes
			name_order = 230
		}
		location = 12304
		division_template = "Colonial Volunteer Fortress Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	division= {
		#British Honduras Garrison
		division_name = {
			is_name_ordered = yes
			name_order = 231
		}
		location = 2077
		division_template = "Small Colonial Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	division= {
		#British Guiana Garrison
		division_name = {
			is_name_ordered = yes
			name_order = 232
		}
		location = 4982
		division_template = "Small Colonial Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	division= {
		#Newfoundland Garrison
		division_name = {
			is_name_ordered = yes
			name_order = 233
		}
		location = 12505
		division_template = "Small Colonial Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	
	##South Atlantic Command###############################
	division= {
		#St Helena Garrison
		division_name = {
			is_name_ordered = yes
			name_order = 234
		}
		location = 13016
		division_template = "Small Colonial Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	division= {
		#Falklands Islands Defence Force
		division_name = {
			is_name_ordered = yes
			name_order = 235
		}
		location = 12960
		division_template = "Small Colonial Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	
	### Far East Command ############################
	division= {
		#Hong Kong Volunteer Defence Corps (HKVDC)
		division_name = {
			is_name_ordered = yes
			name_order = 236
		}
		location = 10062
		division_template = "Colonial Volunteer Fortress Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	division= {
		#Hong Kong Brigade
		division_name = {
			is_name_ordered = yes
			name_order = 237
		}
		location = 10062
		division_template = "Fortress Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.8
	}
	
	###Malaya########################################
	division= {
		#Singapore Garrison
		division_name = {
			is_name_ordered = yes
			name_order = 238
		}
		location = 12299
		division_template = "Fortress Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.8
	}
	#division= {
	#	name = "12th Indian Brigade Group"
	#	location = 12299
	#	division_template = "Infantry Division (ME)"
	#	start_experience_factor = 0.3
	#	start_equipment_factor = 0.5 		# should be event activated
	#
	#}
	#division= {
	#	name = "Straits Settlements Volunteer Force"
	#	location = 12299
	#	division_template = "Colonial Volunteer Fortress Garrison"
	#	start_experience_factor = 0.3
	#	start_equipment_factor = 0.3
	#}
	#division= {
	#	name = "Penang Fortress"
	#	location = 10297
	#	division_template = "Small Colonial Garrison"
	#	start_experience_factor = 0.3
	#	start_equipment_factor = 0.15
	#}
	#division= {
	#	name = "Khota Bharu Garrison"
	#	location = 7329
	#	division_template = "Small Colonial Garrison"
	#	start_experience_factor = 0.3
	#	start_equipment_factor = 0.5
	#}
	#division= {
	#	name = "Sarawak Volunteer Corps"
	#	location = 7371
	#	division_template = "Local Defence Force"
	#	force_equipment_variants = { infantry_equipment_0 = { owner = "ENG" } }
	#	start_experience_factor = 0.1
	#	start_equipment_factor = 0.15
	#}
	division= {
		#British Troops in Borneo
		division_name = {
			is_name_ordered = yes
			name_order = 239
		}
		location = 2904
		division_template = "Small Colonial Garrison"
		force_equipment_variants = { infantry_equipment_0 = { owner = "ENG" } }
		start_experience_factor = 0.1
		start_equipment_factor = 0.15
	}
	#division= {
	#	name = "Khota Bharu Garrison"
	#	location = 7329
	#	division_template = "Small Colonial Garrison"
	#	start_experience_factor = 0.3
	#	start_equipment_factor = 0.5
	#}
	#division= {
	#	name = "Kota Bharu Garrison"
	#	location = 12215
	#	division_template = "Small Colonial Garrison"
	#	start_experience_factor = 0.3
	#	start_equipment_factor = 0.15
	#}
	#division= {
	#	name = "Teluk Anson Garrison"
	#	location = 12215
	#	division_template = "Small Colonial Garrison"
	#	start_experience_factor = 0.3
	#	start_equipment_factor = 0.15
	#}
	division= {
		#Fiji Defence Force
		division_name = {
			is_name_ordered = yes
			name_order = 240
		}
		location = 4286
		division_template = "Small Colonial Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.3
	}
	division= {
		#Ocean Island Defence Force
		division_name = {
			is_name_ordered = yes
			name_order = 241
		}
		location = 4387
		division_template = "Small Colonial Garrison"
		start_experience_factor = 0.3
		start_equipment_factor = 0.15
	}
	division= {
		#Ceylon Defence Force
		division_name = {
			is_name_ordered = yes
			name_order = 23
		}
		location = 7260
		division_template = "Local Defence Force"
		force_equipment_variants = { infantry_equipment_0 = { owner = "ENG" } }
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
	}
	division= {
		#Trincomalee Fortress
		division_name = {
			is_name_ordered = yes
			name_order = 243
		}
		location = 10201
		division_template = "Colonial Volunteer Fortress Garrison"
		force_equipment_variants = { infantry_equipment_0 = { owner = "ENG" } }
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
	}
}

### Airforce OOB ###

air_wings = {
	### RAF Fighter Command ###
	129 = { 
		### No. 11 Fighter Command, Middlesex area -- Hawker Demons, Furys, Gloster Gauntlets
		hawker_demon_equipment_1 = {
			owner = "ENG" 
			amount = 70
		}
		start_experience_factor = 0.2
	}
	130 = { 
		### No. 11 Fighter Command, Essex area -- Hawker Demons, Furys, Gloster Gauntlets
		hawker_demon_equipment_1 = {
			owner = "ENG" 
			amount = 68
		}
		start_experience_factor = 0.2
	}	
	127 = { 
		### No. 12 Fighter Command, Nottingham area -- Hawker Demons, Furys, Gloster Gauntlets
		hawker_fury_equipment_1 = {
			owner = "ENG" 
			amount = 60
		}
		start_experience_factor = 0.2
	}	
	131 = { 
		### No. 13 Fighter Command, Newcastle -- Hawker Demons, Furys, Gloster Gauntlets
		hawker_demon_equipment_1 = { 
			owner = "ENG" 
			amount = 24
		}
		start_experience_factor = 0.2
	}
	### RAF Bomber Command ###
	125 = { 
		### No. 1 Bomber Command, East Anglia area -- Fairey Gordons, Battles
		hawker_hart_equipment_1 = {
			owner = "ENG"
			amount = 64
		}
		start_experience_factor = 0.2
		hawker_hind_equipment_1 = { #First batch
			owner = "ENG"
			amount = 20
		}
		start_experience_factor = 0.2
	}
	125 = { 
		### No. 2 Bomber Command, East Anglia area -- HP Heyford, Harrows, AW Whitleys
		handley_heyford_equipment_1 = { # Heyford I
			owner = "ENG"
			amount = 15
		}
		start_experience_factor = 0.2
		vickers_virginia_bomber_equipment_1 = { # Vickers Virginia IX
			owner = "ENG" 
			amount = 19
		}
		start_experience_factor = 0.2
	}
	126 = { 
		### No. 3 Bomber Command, Suffolk area -- HP Heyford, Harrows, AW Whitleys
		handley_heyford_equipment_2 = { # heyford Ia
			owner = "ENG"
			amount = 23
		}
		start_experience_factor = 0.2
		vickers_virginia_bomber_equipment_2 = {	# Vickers Virginia X
			owner = "ENG"
			amount = 52
		}
		start_experience_factor = 0.2
	}
	130 = { 
		### No. 4 Bomber Command, York area -- HP Heyford, Harrows, AW Whitleys
		handley_heyford_equipment_3 = { # Heyford II
			owner = "ENG"
			amount = 17
		}
		start_experience_factor = 0.2
		vickers_virginia_bomber_equipment_2 = { # Vickers Virginia X
			owner = "ENG"
			amount = 52
		}
		start_experience_factor = 0.2
		vickers_valentia_transport_equipment_1 = {
			owner = "ENG"
			amount = 25
		}
		start_experience_factor = 0.2
	}
	128 = { 
		### No. 5 Bomber Command, Lincoln area -- Fairey Gordons, Battles
		hawker_hart_equipment_1 = {
			owner = "ENG"
			amount = 64
		}
		start_experience_factor = 0.2
		bolton_overstrand_equipment_1 = {	#Boulton Paul Overstrand
			owner = "ENG"
			amount = 24
		}
		start_experience_factor = 0.2
	}
	### RAF Coastal Command
	130 = { 
		### No. 16 Coastal Command -- Vickers Vildebeests
		supermarine_scapa_equipment_1 = {	#Supermarine Scapa
			owner = "ENG"			
			amount = 15
		}
		start_experience_factor = 0.16
		supermarine_southampton_equipment_1 = {	#Supermarine Southampton
			owner = "ENG"			
			amount = 30
		}
		start_experience_factor = 0.16
	}
	### RAF Middle East and Africa
	447 = { 
		### AHQ Egypt
		hawker_fury_equipment_1 = {
			owner = "ENG" 
			amount = 40
		}
		start_experience_factor = 0.16
		vickers_valentia_equipment_1 = {
			owner = "ENG" 
			amount = 82
		}
		start_experience_factor = 0.16
		hawker_hart_equipment_1 = {
			owner = "ENG"			
			amount = 64
		}
		start_experience_factor = 0.16
		hawker_audax_equipment_1 = { #Guestimates Hawker Audax
			owner = "ENG"
			amount = 60
		}
		start_experience_factor = 0.16
		fairey_gordon_equipment_1 = { #Guestimates	#Fairey Gordon
			owner = "ENG"
			amount = 20
		}
		start_experience_factor = 0.16
	}
	454 = { 
		### RAF Palestine & Iraq
		hawker_hart_equipment_1 = {
			owner = "ENG"
			amount = 47
		}
		start_experience_factor = 0.16
		hawker_hart_equipment_1 = {  # Hawker Hart Special #Includes 14 as part of egyptian air force
			owner = "ENG"
			amount = 30
		}
		start_experience_factor = 0.16
		hawker_audax_equipment_1 = { #Guestimates
			owner = "ENG"
			amount = 60
		}
		start_experience_factor = 0.16
		hawker_hart_equipment_1 = { #Hardy, only a slight difference in range
			owner = "ENG"
			amount = 48
		}
		start_experience_factor = 0.16
	}
	659 = { 
		### RAF Aden
		hawker_fury_equipment_1 = {
			owner = "ENG" 
			amount = 16
		}
		start_experience_factor = 0.16
		hawker_hart_equipment_1 = {
			owner = "ENG" 
			amount = 47
		}
		start_experience_factor = 0.16
	}
	### RAF Far East
	336 = { 
		### RAF Far East
		#nav_bomber_equipment_1936 = {
		#	owner = "ENG" 
		#	amount = 24
		#}
		hawker_hart_equipment_1 = {
			owner = "ENG"
			amount = 64
		}
		start_experience_factor = 0.16
		hawker_hart_equipment_1 = { #Technically in india
			owner = "ENG"
			amount = 52
		}
		start_experience_factor = 0.16
		westland_wapiti_equipment_1 = { #Technically in india, unsure of amount #Wapiti
			owner = "ENG"
			amount = 27 #At least this many served with indian air units
		}
		start_experience_factor = 0.16
	}
}

#############################
###### STARTING PRODUCTION ##
#############################
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "ENG"
		}
		requested_factories = 6
		progress = 0.1
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "ENG" 
		}
		requested_factories = 4
		progress = 0.3
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = garrison_equipment_0
			creator = "ENG" 
		}
		requested_factories = 4
		progress = 0.3
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = artillery_equipment_1
			creator = "ENG" 
		}
		requested_factories = 2
		progress = 0.3
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = motorized_equipment_2
			creator = "ENG" 
		}
		requested_factories = 2
		progress = 0.3
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = infantry_uniforms_1
			creator = "ENG" 
		}
		requested_factories = 2
		progress = 0.3
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = radio_equipment_0
			creator = "ENG" 
		}
		requested_factories = 2
		progress = 0.3
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = trm_light_tank_chassis_eng_vickers_light_3 
			creator = "ENG" 
			version_name = "Light Mk.V"
		}
		requested_factories = 2
		progress = 0.4
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = hawker_demon_equipment_1
			creator = "ENG" 
		}
		requested_factories = 2
		progress = 0.15
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = hawker_hind_equipment_1
			creator = "ENG" 
		}
		requested_factories = 2
		progress = 0.1
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = hawker_hart_equipment_1
			creator = "ENG" 
		}
		requested_factories = 2
		progress = 0.1
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = hawker_audax_equipment_1
			creator = "ENG" 
		}
		requested_factories = 2
		progress = 0.1
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = HMG_equipment_0
			creator = "ENG"
		}
		requested_factories = 2
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = mortar_equipment_0
			creator = "ENG"
		}
		requested_factories = 2
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = Hvartillery_equipment_0
			creator = "ENG"
		}
		requested_factories = 2
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = artyhorse_equipment_0
			creator = "ENG"
		}
		requested_factories = 2
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = artytruck_equipment_0
			creator = "ENG"
		}
		requested_factories = 2
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = recon_equipment_0
			creator = "ENG"
		}
		requested_factories = 2
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = anti_tank_equipment_1
			creator = "ENG"
		}
		requested_factories = 2
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = anti_air_equipment_1
			creator = "ENG"
		}
		requested_factories = 2
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = artytractor_equipment_0
			creator = "ENG"
		}
		requested_factories = 2
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = motorized_lia_equipment_01
			creator = "ENG"
		}
		requested_factories = 2
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = train_equipment_1
			creator = "ENG"
		}
		requested_factories = 2
		progress = 0.85
		efficiency = 100
	}
}
#####################################
########## Stockpiled equipment #####
instant_effect = {
	add_equipment_to_stockpile = {
		type = nimrod_fighter_equipment_1
		amount = 23
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = infantry_equipment_1
		amount = 10000
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = garrison_equipment_0
		amount = 2000
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = infantry_uniforms_0
		amount = 8000
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = support_equipment_1
		amount = 2500
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = motorized_equipment_2
		amount = 1500
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = artytruck_equipment_0
		amount = 300
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = horse_equipment_0
		amount = 1600
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = HMG_equipment_0
		amount = 1700
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = mortar_equipment_0
		amount = 1700
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = SMG_equipment_0
		amount = 3000
		producer = USA
	}
	# Tank stockpile in country file
}
### EOF ###