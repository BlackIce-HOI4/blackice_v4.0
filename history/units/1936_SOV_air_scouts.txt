air_wings = {

	# =============================================
	# Leningrad MD - Leningrad (195)               
	# =============================================
	#
	# - 1st "im.Mezheraup" Heavy Bomber Brigade - Krechevitsy (263)
	#   + 2nd LR Recon Sqn - 12 R-6
	# - 53rd Light Bomber Brigade - Krasnogvardeysk (195)
	#   + 1st "im.Kirova" LR Recon Sqn - 12 R-6

	263 = { 
		r_6_scout_equipment_1 = {
			owner = "SOV" 
			amount = 12
		}
		name="2 DRAE"
		start_experience_factor = 0.16
	}

	195 = { 
		r_6_scout_equipment_1 = {
			owner = "SOV" 
			amount = 12
		}
		name="1 DRAE 'im.Kirova'"
		start_experience_factor = 0.16
	}

	# =============================================
	# Belarus MD - Minsk (206)
	# =============================================
	# - 116th "im.VCSPS" Mixed Brigade - Smolensk (242)
	#   + 4th LR Recon Sqn - 12 R-6
	#   + 5th LR Recon Sqn - 12 R-6

	242 = { 
		r_6_scout_equipment_1 = {
			owner = "SOV" 
			amount = 12
		}
		name="4 DRAE"
		start_experience_factor = 0.16

		r_6_scout_equipment_1 = {
			owner = "SOV" 
			amount = 12
		}
		name="5 DRAE"
		start_experience_factor = 0.16
	}

	# =============================================
	# Moscow MD - Moscow (845)
	# =============================================
	# - 18th LR Recon Sqn - 12 R-6

	845 = { 
		r_6_scout_equipment_1 = {
			owner = "SOV" 
			amount = 12
		}
		name="18 DRAE"
		start_experience_factor = 0.16
	}

	# =============================================
	# Kharkov MD - Kharkov (221)
	# =============================================
	# - 43rd Mixed Brigade - Kharkov (221)
	#   + 6th LR Recon Sqn - 12 R-6
	#   + 9th LR Recon Sqn - 12 R-6

	221 = { 
		r_6_scout_equipment_1 = {
			owner = "SOV" 
			amount = 12
		}
		name="6 DRAE"
		start_experience_factor = 0.16

		r_6_scout_equipment_1 = {
			owner = "SOV" 
			amount = 12
		}
		name="9 DRAE"
		start_experience_factor = 0.16
	}

	# =============================================
	# Independent Red Banner Far East Army - Khabarovsk (Vladivostok 408)
	# =============================================
	# - 15th LR Recon Sqn - 12

	408 = { 
		r_6_scout_equipment_1 = {
			owner = "SOV" 
			amount = 12
		}
		name="15 DRAE"
		start_experience_factor = 0.16
	}
}
