﻿
units = {
	division= {	
		name = "SAS"
		location = 6536
		division_template = "Commandoes"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}
	division= {	
		name = "SAS"
		location = 6449
		division_template = "Commandoes"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}
	division= {	
		name = "SAS"
		location = 11616
		division_template = "Commandoes"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}
	division= {	
		name = "SAS"
		location = 11548
		division_template = "Commandoes"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}
	division= {	
		name = "SAS"
		location = 11318
		division_template = "Commandoes"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}
}
### EOF ###