﻿
units = {

	division= {	
		name = "116. Panzer Div 'Windhund'" 
		#unique = { "Gerhard Müller" "Gerhard Graf von Schwerin" "Heinrich Voigtsberger" "Siegfried von Waldenburg" }
		location  = 6521
		division_template = "Panzer-Division II '1940'"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
}