﻿division_template = {
	name = "15 Pz-Div.(DAK)"		
	template_counter = 694
	priority = 2
	
	regiments = {
		trm_medium_armor = { x = 0 y = 0 }
		trm_medium_cs_armor = { x = 0 y = 1 }
		
		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		
		motorized = { x = 2 y = 0 }
		motorized = { x = 2 y = 1 }
		
		artillery_brigade_mot = { x = 3 y = 0 }
		artillery_brigade_mot = { x = 3 y = 1 }
		artillery_brigade_mot = { x = 3 y = 2 }
		anti_tank_brigade =  { x = 3 y = 3 }
	}
	
	support = {
		DIV_HQ_mech = { x = 0 y = 0 }
		combat_engineer_arm = { x = 0 y = 1 }
        artillery_heavy_mot = { x = 0 y = 2 }
		recon_mot = { x = 0 y = 3 }
		
		maintenance_company = { x = 1 y = 0 }
		logistics_company_mot = { x = 1 y = 1 }
		field_hospital = { x = 1 y = 2 }
		desert_support = { x = 1 y = 3 }
		signal_company_mot = { x = 1 y = 4 }
	}
}

units = {

	division= {	
		name = "15. Panzer-Division" 
		#unique = { "Friedrich Kühn" "Heinrich von Prittwitz und Gaffron" "Walter Hugo Reinahrd Neumann-Silkow" "Gustav von Vaerst" "Eduard Crasemann" "Heinz von Randow" "Willibald Borowitz" }
		location  = 1149
		division_template = "15 Pz-Div.(DAK)"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
}