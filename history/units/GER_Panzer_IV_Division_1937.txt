﻿division_template = {
	name = "Panzer-Division '1937'-"
	division_names_group = GER_Arm_01
	priority = 2
	template_counter = 258
	
	regiments = {
		motorcycle_infantry = { x = 0 y = 0 }
		motorized = { x = 0 y = 1 }
		motorized = { x = 0 y = 2 }
		
		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }

		trm_medium_cs_armor = { x = 2 y = 0 }
		trm_medium_cs_armor = { x = 2 y = 1 }
		
		trm_medium_cs_armor = { x = 3 y = 0 }
		trm_medium_cs_armor = { x = 3 y = 1 }
		
		artillery_brigade_mot = { x = 4 y = 0 }	
		artillery_brigade_mot = { x = 4 y = 1 }
		artillery_brigade_mot = { x = 4 y = 2 }
		anti_tank_brigade_mot =  { x = 4 y = 3 }
	}
	support = {
        DIV_HQ_car = { x = 0 y = 0 }
		engineer_mot = { x = 0 y = 1 }
       	artillery_heavy_mot = { x = 0 y = 2 }
		recon_ac = { x = 0 y = 3 }
		maintenance_company = { x = 0 y = 4 }
		logistics_company_mot = { x = 1 y = 0 }
	}
}
