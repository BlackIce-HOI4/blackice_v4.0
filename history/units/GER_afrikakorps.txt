﻿division_template = {
	name = "Leicht Pz-Div.(DAK)"		
	template_counter = 676
	priority = 2
	
	regiments = {
		trm_medium_armor = { x = 0 y = 0 }
		trm_medium_cs_armor = { x = 0 y = 1 }
		
		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		
		artillery_brigade_mot = { x = 2 y = 0 }
	}
	
	support = {
		DIV_HQ_mech = { x = 0 y = 0 }
		combat_engineer_arm = { x = 0 y = 1 }
        artillery_heavy_mot = { x = 0 y = 2 }
		recon_ac = { x = 0 y = 3 }
		
		maintenance_company = { x = 1 y = 0 }
		logistics_company_mot = { x = 1 y = 1 }
		field_hospital = { x = 1 y = 2 }
		desert_support = { x = 1 y = 3 }
		signal_company_mot = { x = 1 y = 4 }
	}
}

units = {

	division= {	
		name = "5. leichte Afrika-Division" 
		#unique = { "Hans Freiherr von Funck" "Johannes Streich" "Karl Böttcher"}
		location = 1149
		division_template = "Leicht Pz-Div.(DAK)"
		start_experience_factor = 0.7
		start_equipment_factor = 0.01
	}
}
#####################
