﻿
units = {

	division = {
		name = "17. SS-PzG-Div 'Götz von Berlichingen'"
		#unique = { "Werner Ostendorff" "Fritz Klingenberg" "Otto Binge" "Dr. Eduard Deisenhofer" "Thomas Müller" "Gustav Mertsch" "Hans Lingner" "Gerhard Lindner" "Fritz Klingenberg" }
		location = 6521
		division_template = "SS PzG-Div II"
		start_experience_factor = 0.7
		start_equipment_factor = 0.01
	}
}
#####################