﻿

units = {

	division = {
		name = "18. SS-Fre.PzG.Div. 'Horst Wessel'"
		unqiue = { "Wilhelm Trabant" "Josef Fitzthum" "Georg Bochmann" "Heinrich Petersen" }
		location = 6521
		division_template = "SS PzG-Div III"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
}
#####################