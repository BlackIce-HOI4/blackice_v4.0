﻿instant_effect = {
	if = {
		limit = {
			is_ai = yes
		}
		set_research_slots = 0
		country_event = { id = AI_techs.1 days = 50 random_days = 7 }
	}
}	
division_template = {
	name = "Juntuán"				
	
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
		recon_cav = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Da Han Yi Jun"	

	priority = 0
	
	regiments = {
		irregulars_unit = { x = 0 y = 0 }
		irregulars_unit = { x = 0 y = 1 }
		irregulars_unit = { x = 1 y = 0 }
		irregulars_unit = { x = 1 y = 1 }
		militia = { x = 2 y = 0 }
		militia = { x = 2 y = 1 }
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
	}
}

units = {
	division = {
		name = "1 Juntuán"
		location = 4495
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "2 Juntuán"
		location = 1489
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "3 Juntuán"
		location = 10397
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "Da Han Yi Jun"
		location = 4555
		division_template = "Da Han Yi Jun"
		start_experience_factor = 0.1
		start_equipment_factor = 0.6
	}
}
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_0
			creator = "MEN"
		}
		requested_factories = 1
		progress = 0.52
		efficiency = 100
	}
}