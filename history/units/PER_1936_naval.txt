﻿units = {

	##### NAVAL UNITS #####
	fleet = {
		name = "Narwa Draaaa Shahnshaha Aaran"			
		naval_base = 7982 # Bandar e Abbas
		task_force = {
			name = "Narwa Draaaa Shahnshaha Aaran"
			location =  7982 # Bandar e Abbas
			ship = { name = "IIS Babr" definition = escort start_experience_factor = 0.1 equipment = { nrm_ship_hull_light_1 = { amount = 1 owner = PER version_name = "Babr Class" } } }
			ship = { name = "IIS Palang" definition = escort start_experience_factor = 0.1 equipment = { nrm_ship_hull_light_1 = { amount = 1 owner = PER version_name = "Babr Class" } } }
		}					
	}
}

