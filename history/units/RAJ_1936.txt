﻿####################
###### PLAYER ######
####################

division_template = {
	name = "Indian Infantry Division '1936'"		
	division_names_group = RAJ_INF_01				
	
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		militia = { x = 0 y = 2 }
		
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		militia = { x = 1 y = 2 }
		
		cavalry = { x = 2 y = 0 }
		cavalry = { x = 2 y = 1 }
		cavalry = { x = 2 y = 2 }
		
		gurkha = { x = 3 y = 0 }
		
		mountain_artillery_brigade = { x = 4 y = 0 }
		mountain_artillery_brigade = { x = 4 y = 1 }
		mountain_artillery_brigade = { x = 4 y = 2 }
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		recon_cav = { x = 0 y = 1 }
	}
}
division_template = {
	name = "District Division"		
	division_names_group = RAJ_GAR_01	
	priority = 0
	
	regiments = {
		cavalry = { x = 0 y = 0 }
		
		gurkha = { x = 1 y = 0 }
		
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		militia = { x = 2 y = 2 }
		
		artillery_brigade = { x = 3 y = 0 }       
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
		recon_cav = { x = 0 y = 1 }   
	}
}
division_template = {
	name = "Indian Cavalry Division"		
	division_names_group = RAJ_CAV_01	
	
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		
		semi_motorized = { x = 1 y = 0 }
		semi_motorized = { x = 1 y = 1 }
		semi_motorized = { x = 1 y = 2 }     
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
		recon_cav = { x = 0 y = 1 }
	}
}
# Not active 1936
#division_template = {
#	name = "Indian Armoured Division"		
#	division_names_group = RAJ_ARM_01	
#	
#	regiments = {
#		trm_light_armor = { x = 0 y = 0 }
#		trm_light_armor = { x = 0 y = 1 }
#		
#		semi_motorized = { x = 1 y = 0 }
#		semi_motorized = { x = 1 y = 1 }
#		semi_motorized = { x = 1 y = 2 }
#	}
#	support = {
#		DIV_HQ = { x = 0 y = 0 }
#		recon_ac = { x = 0 y = 1 }
#	}
#}
division_template = {
	name = "Bodyguard"
	#division_names_group =
	
	regiments = {
		cavalry = { x = 0 y = 0 }		
		cavalry = { x = 0 y = 1 }
		
		gurkha = { x = 1 y = 0 }
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
		military_police = { x = 0 y = 1 }
		recon_mot = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Garrison"			
	division_names_group = RAJ_GAR_01
	
	regiments = {
		garrison = { x = 0 y = 0 }
		garrison = { x = 0 y = 1 }
		garrison = { x = 0 y = 2 }
		
		garrison = { x = 1 y = 0 }
		garrison = { x = 1 y = 1 }
		garrison = { x = 1 y = 2 }
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
		recon_cav = { x = 0 y = 1 }
	}
}

#### OOB ####
units = {
	#India Command
	#division= {
	#	name = "Army of India" 
	#	location = 10843
	#	division_template = "HQ Command" 
	#	start_experience_factor = 0.0
	#	start_equipment_factor = 0.1
	#}
	
	#Northern Command
	
	# Not active 1936
	#division= {
	#	name = "The Royal Tank Regiment"
	#	location = 12717
	#	division_template = "Indian Armoured Division"
	#	start_experience_factor = 0.4
	#	start_equipment_factor = 0.7
	#	force_equipment_variants = { 
	#		trm_light_tank_chassis_eng_vickers_light_2 = { owner = "ENG" version_name = "Light Mk.IV" }
	#	}
	#}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 101
		}
		location = 12717
		division_template = "Indian Infantry Division '1936'"
		start_experience_factor = 0.4
		start_equipment_factor = 0.7
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 12717
		division_template = "Indian Cavalry Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.5
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 102
		}
		location = 4998
		division_template = "Indian Infantry Division '1936'"
		start_experience_factor = 0.5
		start_equipment_factor = 0.7
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 103
		}
		location = 12005
		division_template = "Indian Infantry Division '1936'"
		start_experience_factor = 0.5
		start_equipment_factor = 0.7
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 104
		}
		location = 10843
		division_template = "Indian Infantry Division '1936'"
		start_experience_factor = 0.5
		start_equipment_factor = 0.7
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 105
		}
		location = 1978
		division_template = "Indian Infantry Division '1936'"
		start_experience_factor = 0.5
		start_equipment_factor = 0.7
	}
	
	#Eastern Command
	division= {
		name = "The Governor General's Bodyguard" 
		location = 10843
		division_template = "Bodyguard" 
		start_experience_factor = 0.7
		#start_equipment_factor = 0.9
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 106
		}
		location = 11955
		division_template = "Indian Infantry Division '1936'"
		start_experience_factor = 0.5
		start_equipment_factor = 0.5
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 11955
		division_template = "Indian Cavalry Division"
		start_experience_factor = 0.5
		start_equipment_factor = 0.5
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 107
		}
		location = 12137
		division_template = "Indian Infantry Division '1936'"
		start_experience_factor = 0.5
		start_equipment_factor = 0.6
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 108
		}
		location = 1497
		division_template = "District Division"
		start_experience_factor = 0.6
		start_equipment_factor = 0.7
	}
	
	#Southern Command
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 109
		}
		location = 12781
		division_template = "District Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.6
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 12781
		division_template = "Indian Cavalry Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.6
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 110
		}
		location = 1349
		division_template = "District Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.6
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 111
		}
		location = 10278
		division_template = "District Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.6
	}
	division= {
		division_name = {
			is_name_ordered = yes
			name_order = 112
		}
		location = 5105
		division_template = "Indian Infantry Division '1936'"
		start_experience_factor = 0.4
		start_equipment_factor = 0.7
	}
}

air_wings = {
	### AHQ India
	439 = {
		### AHQ India -- Fairey Gordons, Battles
		fairey_gordon_equipment_1 = {
			owner = "RAJ"
			amount = 80
		}
	}
}

##### Starting Production #####
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "RAJ"
		}
		requested_factories = 1
		progress = 0.73
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = garrison_equipment_0
			creator = "RAJ"
		}
		requested_factories = 1
		progress = 0.73
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = infantry_uniforms_1
			creator = "RAJ"
		}
		requested_factories = 1
		progress = 0.73
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "RAJ"
		}
		requested_factories = 1
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = recon_equipment_0
			creator = "RAJ"
		}
		requested_factories = 1
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = horse_equipment_0
			creator = "RAJ"
		}
		requested_factories = 1
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = artyhorse_equipment_0
			creator = "RAJ"
		}
		requested_factories = 1
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = artytruck_equipment_0
			creator = "RAJ"
		}
		requested_factories = 1
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = artillery_equipment_0
			creator = "RAJ"
		}
		requested_factories = 1
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = mountain_artillery_equipment_0
			creator = "RAJ"
		}
		requested_factories = 1
		progress = 0.85
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = mount_equipment_0
			creator = "RAJ"
		}
		requested_factories = 1
		progress = 0.85
		efficiency = 100
	}
	############################# equipment stockpile
	add_equipment_to_stockpile = {
		type = infantry_equipment_1
			amount = 2500
			producer = ENG
		}
		
	add_equipment_to_stockpile = {
		type = infantry_uniforms_0
			amount = 1500
			producer = ENG
		}
		
	add_equipment_to_stockpile = {
		type = horse_equipment_0
			amount = 1700
			producer = RAJ
		}
}