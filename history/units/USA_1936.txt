﻿instant_effect = {
	set_country_flag = placed_research_centre
	
	add_research_slot = 1
	358 = { state_add_research_centre = yes }
	SLOT:infantry_folder = yes
	SLOT:support_folder = yes
	SLOT:artillery_folder = yes
	SLOT:tank_techs_folder = yes
	SLOT:armor_folder = yes
	SLOT:air_techs_folder = yes
	SLOT:naval_folder = yes
	SLOT:naval_techs_folder = yes
	SLOT:industry_folder = yes
	SLOT:raw_materials_folder = yes
	SLOT:electronics_folder = yes

	add_research_slot = 1
	393 = { state_add_research_centre = yes }
	SLOT:industry_folder = yes
	SLOT:raw_materials_folder = yes
	SLOT:electronics_folder = yes
	
	add_research_slot = 1
	359 = { state_add_research_centre = yes }
	SLOT:naval_folder = yes
	SLOT:naval_techs_folder = yes
	SLOT:naval_doctrine_folder = yes
	
	add_research_slot = 1
	386 = { state_add_research_centre = yes }
	SLOT:air_techs_folder = yes
	SLOT:air_doctrine_folder = yes
}
##### Division Templates #####
division_template = {
	name = "Infantry Division (RA)"  	# Standard formation for frontline divisions after 1940-41
	template_counter = 114
	division_names_group = USA_INF_01
	
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
		
		artillery_brigade_mot = { x = 3 y = 0 }
		artillery_brigade_mot = { x = 3 y = 1 }
		artillery_brigade_mot = { x = 3 y = 2 }
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
		artillery_heavy_mot = { x = 0 y = 1 }  	# US Hvy Field Arty consisted of 2 Bns, 155mm howitzers
		engineer = { x = 0 y = 2 }   		# US Eng Rgt consisted of 2 Bns
		recon_mot = { x = 0 y = 3 }
		maintenance_company = { x = 0 y = 4 }
		logistics_company_car = { x = 1 y = 0 }
		field_hospital = { x = 1 y = 1 }
	}
}
division_template = {
	name = "Infantry Division (NG)"  		# Standard formation for National Guard divisions through late 1940
	priority = 0
	template_counter = 130
	division_names_group = USA_GAR_01
	
	regiments = {						
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
		
		infantry = { x = 3 y = 0 }
		infantry = { x = 3 y = 1 }
		infantry = { x = 3 y = 2 }
		
		artillery_brigade_mot = { x = 4 y = 0 }
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }   # US Eng Rgt consisted of 2 Bns
		recon	 = { x = 0 y = 2 }
		maintenance_company = { x = 0 y = 3 }
		logistics_company = { x = 0 y = 4 }
		field_hospital = { x = 1 y = 0 }
	}
}

division_template = {
	name =  "Territorial Brigade"  		# used for island defense (older equipment, lower experience)
	priority = 0
	template_counter = 84
	
	regiments = {
		garrison = { x = 0 y = 0 }
		garrison = { x = 0 y = 1 }
		
		mountain_artillery_brigade_mot = { x = 1 y = 0 }
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		anti_air = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Marine Brigade"	
	priority = 2
	template_counter = 3002
	
	regiments = {
		marine = { x = 0 y = 0 }
		marine = { x = 0 y = 1 }
		marine = { x = 0 y = 2 }

		marine = { x = 1 y = 0 }
		marine = { x = 1 y = 1 }
		marine = { x = 1 y = 2 }

		mountain_artillery_brigade = { x = 2 y = 0 }
		mountain_artillery_brigade = { x = 2 y = 1 }
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		recon  = { x = 0 y = 2 }
		field_hospital = { x = 0 y = 3 }
		amph_support  = { x = 1 y = 0 }
	}
}
division_template = {
	name = "Marine Division"
	priority = 2
	template_counter = 3003
	division_names_group = USA_MAR_01
	
	regiments = {
		marine = { x = 0 y = 0 }
		marine = { x = 0 y = 1 }
		marine = { x = 0 y = 2 }

		marine = { x = 1 y = 0 }
		marine = { x = 1 y = 1 }
		marine = { x = 1 y = 2 }

		marine = { x = 2 y = 0 }
		marine = { x = 2 y = 1 }
		marine = { x = 2 y = 2 }

		mountain_artillery_brigade_mot = { x = 3 y = 0 }
		mountain_artillery_brigade_mot = { x = 3 y = 1 }
		mountain_artillery_brigade_mot = { x = 3 y = 2 }
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		recon  = { x = 0 y = 2 }
		maintenance_company = { x = 0 y = 3 }
		field_hospital = { x = 0 y = 4 }
		amph_support  = { x = 1 y = 0 }
	}
}
division_template = {
	name =  "Harbor Defenses"  				# used for coastal defenses, 5 were assigned based on the 5 corps ares with sugnificant coastal territory to defend... thus numbers correspond to the corps number instead of being sequential..  there were also three territorial coastal defense batteries set up in Philipines, Hawaii and Panama Canal Zone.
	template_counter = 4000
	
	regiments = {
		garrison = { x = 0 y = 0 }
		garrison = { x = 0 y = 1 }
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
		artillery_heavy = { x = 0 y = 1 }
		anti_air = { x = 0 y = 2 }
		maintenance_company = { x = 0 y = 3 }
	}
}

#################
###### OOB ######
units = {
	####### CONUS Theater #######
	#division= {	
	#	name = "CONUS" 
	#	location = 3878
	#	division_template = "HQ Command" 
	#	start_experience_factor = 0.0
	#	start_equipment_factor = 0.1
	#}
	### 1st Army -- note: Armies only paper organizations in 1936 ###
	# I Army Corps #
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 9
		}
		location = 6732
		division_template = "Infantry Division (RA)"
		start_experience_factor = 0.3
		start_equipment_factor = 0.2
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}	
	
	# II Army Corps #
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}	
		location = 3878
		division_template = "Infantry Division (RA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.9
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}			
		
	# III Army Corps #
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}
		location = 6984
		division_template = "Infantry Division (RA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}

	### 2nd Army -- note: Armies only paper organizations in 1936 ###
	# V Army Corps #
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 6855
		division_template = "Infantry Division (RA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
			
	# VI Army Corps #
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 9450
		division_template = "Infantry Division (RA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}

	### 3rd Army -- note: Armies only paper organizations in 1936 ###
	# IV Army Corps #
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 12384
		division_template = "Infantry Division (RA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
			
	# VIII Army Corps #
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 12782
		division_template = "Infantry Division (RA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}			
	### 4th Army -- note: Armies only paper organizations in 1936 ###
	# VII Army Corps (CO: Bishop) #
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 12586
		division_template = "Infantry Division (RA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.2
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
		
	# IX Army Corps (CO: DeWitt) #
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 9671
		division_template = "Infantry Division (RA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}		

	# 1st Marine Brigade
	division= {			
		name = "1st Marine Brigade"		
		location = 6929
		division_template = "Marine Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}	

	####### Pacific Theater	#######	
	### U.S. Army Forces in the Far East ###		
	
	### Alaska Department ###	
	division= {			
		name = "Attu Island Garrison"		
		location = 13067
		division_template = "Territorial Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
	division= {			
		name = "Alaska Garrison"		
		location = 13091
		division_template = "Territorial Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
	### Hawaiian Department ###
	division = {			
		name = "Hawaiian Division"		
		location = 4180
		division_template = "Infantry Division (RA)"		#It was considered part of the regular army and tasked with defense of hawaiian main islands while the Hawaii NG was tasked with defending the outlying islands.
		start_experience_factor = 0.3
		start_equipment_factor = 0.9
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
	# North Pacific Defenses #
	division= {				
		name = "Midway Island Garrison"			
		location = 13052
		division_template = "Territorial Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}				
	division= {				
		name = "Wake Island Garrison"			
		location = 13047
		division_template = "Territorial Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}				
	division= {				
		name = "Guam Garrison"			
		location = 12140
		division_template = "Territorial Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
	# South Pacific Defenses #
	division= {			
		name = "Christmas Island Garrison"		
		location = 13048
		division_template = "Territorial Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}			
	division= {			
		name = "Jarvis Island Garrison"		
		location = 13049
		division_template = "Territorial Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}			
	division= {			
		name = "Palmyra Garrison"		
		location = 13050
		division_template = "Territorial Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}			
	division= {			
		name = "Phoenix Island Garrison"		
		location = 13053
		division_template = "Territorial Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}			
	division= {			
		name = "Johnston Island Garrison"		
		location = 13051
		division_template = "Territorial Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
    division= {			
		name = "Panama Canal Coastal Battery"		
		location = 7617
		division_template = "Harbor Defenses"
		start_experience_factor = 0.3
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
	division= {			
		name = "Corregidor Coastal Battery"		
		location = 10265
		division_template = "Harbor Defenses"
		start_experience_factor = 0.3
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
	division= {			
		name = "Pearl Harbor Coastal Battery"		
		location = 4180
		division_template = "Harbor Defenses"
		start_experience_factor = 0.3
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
}

######################################################
#### PLAYER AND AI HAVE SEPERATE NAVAL & AIR OOBS ####
######################################################

instant_effect = {
	if = {
		limit = {
			USA = { is_ai = yes }
			NOT = {	
				has_game_rule = {
					rule = AI_country_tech_rules
					option = rule_country_specific_1
				}
			}
		}
		USA = { 
			#set_country_flag = USA_AI 
			load_oob = "1936_USA_AI" 
		}
	}
	if = {
		limit = {
			OR = {
				USA = { is_ai = no }
				has_game_rule = {
					rule = AI_country_tech_rules
					option = rule_country_specific_1
				}
			}
		}
		USA = { load_oob = "1936_USA_player" } 
	}
}

### EOF ##################