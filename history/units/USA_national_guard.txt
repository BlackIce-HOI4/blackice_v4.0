﻿##### Division Templates #####
	
division_template = {
	name = "Infantry Division (NG)"  		# Standard formation for National Guard divisions through late 1940
	priority = 0
	template_counter = 130
	division_names_group = USA_GAR_01
	
	regiments = {						
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
		
		infantry = { x = 3 y = 0 }
		infantry = { x = 3 y = 1 }
		infantry = { x = 3 y = 2 }
		
		artillery_brigade_mot = { x = 4 y = 0 }
		artillery_brigade_mot = { x = 4 y = 1 }
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }   # US Eng Rgt consisted of 2 Bns
		recon	 = { x = 0 y = 2 }
		
		maintenance_company = { x = 1 y = 0 }
		logistics_company = { x = 1 y = 1 }
		field_hospital = { x = 1 y = 2 }
	}
	
}
units = {
    division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 28
		}
		location = 6984
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
	#division = {										
	#	name = "Phillippine Division"			
	#	location = 10265
	#	division_template = "Infantry Division (NG)"			
	#	start_experience_factor = 0.3
	#	start_equipment_factor = 0.01
	#	force_equipment_variants = { 
	#		infantry_equipment_0 = { owner = "USA" }
	#	}
	#}			
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 29
		}
		location = 6984
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
    division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 32
		}
		location = 9450
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}			
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 33
		}
		location = 9450
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
	
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 36
		}
		location = 12782
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}			
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 45
		}
		location = 12782
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
	division = {			
		division_name = {
			is_name_ordered = yes
			name_order = 30
		}
		location = 12384
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}	
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 37
		}
		location = 6855
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}			
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 38
		}
		location = 6855
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}		
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 31
		}
		location = 12384
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 27
		}
		location = 3878
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}			
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 44
		}
		location = 3878
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}	
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 26
		}
		location = 6732
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}			
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 43
		}
		location = 6732
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 34
		}
		location = 12586
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}			
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 35
		}
		location = 12586
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}	
	division = {			
		division_name = {
			is_name_ordered = yes
			name_order = 40
		}
		location = 9671
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}			
	division= {			
		division_name = {
			is_name_ordered = yes
			name_order = 41
		}
		location = 9671
		division_template = "Infantry Division (NG)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
		force_equipment_variants = { 
			infantry_equipment_0 = { owner = "USA" }
		}
	}
	### Carribean Forces ###
	#division = {			
	#	name = "Puerto Rico Division"
	#	location = 1440
	#	division_template = "Infantry Division (NG)"
	#	start_experience_factor = 0.2
	#	start_equipment_factor = 0.01
	#	force_equipment_variants = { 
	#		infantry_equipment_0 = { owner = "USA" }
	#	}
	#}			
	#division = {			
	#	name = "Panama Canal Division"		
	#	location = 7617
	#	division_template = "Infantry Division (NG)"
	#	start_experience_factor = 0.2
	#	start_equipment_factor = 0.01
	#	force_equipment_variants = { 
	#		infantry_equipment_0 = { owner = "USA" }
	#	}
	#}
}
