### North Carolina Class ###
instant_effect = {
	set_country_flag = HSD_USA_BB_north_carolina_oob
	add_equipment_production = {
		equipment = {
			type = nrm_ship_hull_capital_3
			creator = "USA" 
			version_name = "North Carolina Class"
		}
		name = "USS North Carolina"
		requested_factories = 1
		progress = 0.26
		amount = 1
	}
	add_equipment_production = {
		equipment = {
			type = nrm_ship_hull_capital_3
			creator = "USA" 
			version_name = "North Carolina Class"
		}
		name = "USS Washington"
		requested_factories = 1
		progress = 0.26
		amount = 1
	}
}
### EOF ###