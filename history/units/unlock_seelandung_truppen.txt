﻿division_template = {
	name = "Seelandungs-Brigade"
	template_counter = 969 # With staff Marines-Bde
	
	regiments = {
		marine = { x = 0 y = 0 }
		marine = { x = 0 y = 1 }
		
		marine = { x = 1 y = 0 }
		marine = { x = 1 y = 1 }
	}
	support = {
		DIV_HQ = { x = 0 y = 0 }
	}
}
### EOF ###