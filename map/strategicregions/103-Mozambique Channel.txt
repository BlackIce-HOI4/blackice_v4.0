strategic_region={
	id=103
	name="STRATEGICREGION_103"
	provinces={
		2409 2432 2456 2480 5247 5299 5325 5348 5370 5543 5619 8691 
	}
	#naval_terrain = ocean
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ 31.2 32.7 }
			no_phenomenon=0.250
			rain_light=0.177
			rain_heavy=0.106
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 29.9 36.1 }
			no_phenomenon=0.300
			rain_light=0.260
			rain_heavy=0.155
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 29.2 32.2 }
			no_phenomenon=0.350
			rain_light=0.095
			rain_heavy=0.057
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 26.7 29.7 }
			no_phenomenon=0.375
			rain_light=0.160
			rain_heavy=0.096
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 23.3 27.7 }
			no_phenomenon=0.375
			rain_light=0.095
			rain_heavy=0.057
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 20.3 25.7 }
			no_phenomenon=0.450
			rain_light=0.292
			rain_heavy=0.175
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 21.3 25.7 }
			no_phenomenon=0.500
			rain_light=0.075
			rain_heavy=0.045
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 22.3 27.2 }
			no_phenomenon=0.450
			rain_light=0.223
			rain_heavy=0.134
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 22.8 28.7 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.077
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 24.3 30.2 }
			no_phenomenon=0.325
			rain_light=0.297
			rain_heavy=0.178
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 24.8 30.2 }
			no_phenomenon=0.275
			rain_light=0.242
			rain_heavy=0.145
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ 26.2 32.7 }
			no_phenomenon=0.250
			rain_light=0.120
			rain_heavy=0.072
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
