strategic_region={
	id=109
	name="STRATEGICREGION_109"
	provinces={
		59 842 1270 1495 1720 2384 2410 4036 4259 4485 4708 4931 5023 5142 5452 5906 5954 5964 5978 8706 8746 8915 8938 9091 
	}
	#naval_terrain = ocean
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ 25.5 26.6 }
			no_phenomenon=0.250
			rain_light=0.177
			rain_heavy=0.106
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 25.3 28.7 }
			no_phenomenon=0.300
			rain_light=0.260
			rain_heavy=0.155
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 22.7 26.5 }
			no_phenomenon=0.350
			rain_light=0.095
			rain_heavy=0.057
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 19.8 23.6 }
			no_phenomenon=0.375
			rain_light=0.160
			rain_heavy=0.096
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 15.9 19.1 }
			no_phenomenon=0.375
			rain_light=0.095
			rain_heavy=0.057
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 12.1 16.3 }
			no_phenomenon=0.450
			rain_light=0.292
			rain_heavy=0.175
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 11.4 14.6 }
			no_phenomenon=0.500
			rain_light=0.075
			rain_heavy=0.045
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 12.4 16.5 }
			no_phenomenon=0.450
			rain_light=0.223
			rain_heavy=0.134
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 14.2 18.4 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.077
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 17.8 20.7 }
			no_phenomenon=0.325
			rain_light=0.297
			rain_heavy=0.178
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 21.2 22.8 }
			no_phenomenon=0.275
			rain_light=0.242
			rain_heavy=0.145
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ 22.3 26.6 }
			no_phenomenon=0.250
			rain_light=0.120
			rain_heavy=0.072
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
