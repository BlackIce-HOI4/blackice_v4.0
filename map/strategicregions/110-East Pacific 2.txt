strategic_region={
	id=110
	name="STRATEGICREGION_110"
	provinces={
		267 465 647 1049 2148 2254 2307 2380 2406 2430 2454 2505 3051 3252 3453 3647 3841 4048 4271 4497 4721 4944 5147 5149 5256 5281 5308 5334 5357 5358 5380 5405 5429 5477 5503 5528 5529 5553 5577 5603 5629 5654 5679 5705 5731 5757 5758 5784 5789 5810 5815 5836 5841 5862 5867 5888 5893 5911 5916 5935 5940 5959 5983 6006 6426 6847 7051 
	}
	naval_terrain=water_deep_ocean
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ 27.0 28.2 }
			no_phenomenon=0.250
			rain_light=0.177
			rain_heavy=0.006
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 26.3 30.9 }
			no_phenomenon=0.300
			rain_light=0.260
			rain_heavy=0.013
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 24.7 28.0 }
			no_phenomenon=0.350
			rain_light=0.095
			rain_heavy=0.024
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 22.2 25.4 }
			no_phenomenon=0.375
			rain_light=0.160
			rain_heavy=0.047
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 18.7 22.3 }
			no_phenomenon=0.375
			rain_light=0.095
			rain_heavy=0.057
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 15.5 20.0 }
			no_phenomenon=0.450
			rain_light=0.292
			rain_heavy=0.175
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 15.6 19.2 }
			no_phenomenon=0.500
			rain_light=0.075
			rain_heavy=0.045
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 16.6 20.9 }
			no_phenomenon=0.450
			rain_light=0.223
			rain_heavy=0.134
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 17.6 22.5 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.077
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 20.0 24.3 }
			no_phenomenon=0.325
			rain_light=0.297
			rain_heavy=0.107
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 21.9 25.3 }
			no_phenomenon=0.275
			rain_light=0.242
			rain_heavy=0.031
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ 23.1 28.2 }
			no_phenomenon=0.250
			rain_light=0.120
			rain_heavy=0.013
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
