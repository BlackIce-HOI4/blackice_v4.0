
strategic_region={
	id=137
	name="STRATEGICREGION_137"
	provinces={
227 298 338 339 365 393 410 432 452 464 468 470 477 497 499 508 545 554 566 576 615 618 757 1210 1254 1290 1318 1350 1378 3216 3265 3330 3357 3385 3404 3406 3435 3459 3461 3467 3486 3487 3490 3502 3515 3538 3551 3563 3618 3622 3726 4322 4324 4351 6275 6339 6379 6404 6424 6430 6473 6486 6491 6498 6502 6514 6515 6516 6523 6525 6551 6564 6574 6588 6601 6615 6620 6758 6760 7322 7343 7438 9243 9271 9299 9337 9365 9391 9420 9432 9440 9445 9447 9453 9455 9462 9469 9473 9487 9513 9526 9528 9538 9549 9577 9592 9735 9753 10299 11225 11256 11281 11321 11348 11377 11421 11429 11433 11442 11455 11457 11458 11460 11469 11472 11473 11480 11482 11510 11523 11524 11537 11538 11552 11578 11579 11591 11706 11736 12128 12242 12270 12298 }
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -28.3 0.4 }
			no_phenomenon=0.250
			rain_light=0.118
			rain_heavy=0.071
			snow=0.600
			blizzard=0.200
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ -27.3 1.5 }
			no_phenomenon=0.300
			rain_light=0.100
			rain_heavy=0.060
			snow=0.550
			blizzard=0.200
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ -18.2 9.2 }
			no_phenomenon=0.350
			rain_light=0.147
			rain_heavy=0.088
			snow=0.400
			blizzard=0.100
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ -4.8 22.8 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.077
			snow=0.300
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 5.2 32.7 }
			no_phenomenon=0.375
			rain_light=0.158
			rain_heavy=0.090
			snow=0.100
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 10.5 37.2 }
			no_phenomenon=0.450
			rain_light=0.158
			rain_heavy=0.090
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 14.8 40.6 }
			no_phenomenon=0.500
			rain_light=0.127
			rain_heavy=0.076
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 13.8 39.0 }
			no_phenomenon=0.450
			rain_light=0.109
			rain_heavy=0.065
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 5.0 30.7 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.077
			snow=0.100
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ -4.9 20.4 }
			no_phenomenon=0.325
			rain_light=0.104
			rain_heavy=0.062
			snow=0.300
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ -13.9 11.1 }
			no_phenomenon=0.275
			rain_light=0.113
			rain_heavy=0.068
			snow=0.500
			blizzard=0.200
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -22.3 4.0 }
			no_phenomenon=0.250
			rain_light=0.117
			rain_heavy=0.070
			snow=0.600
			blizzard=0.200
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
