strategic_region={
	id=165
	name="STRATEGICREGION_165"
	provinces={
#Guangdong
		1047 1087 1187 4092 7152 11938 11941 #Yuehai / Guangzhou
		1078 1162 1202 4050 4165 4207 7067 7108 7141 7182 9938 9978 12095#Chaozun 
		1120 9970 9997 10080 10121 12014 #Lingnan
		10062 #Hongkong
		4189 #Macau
		11981 # Guangzhouwan
		1131 4160 7039 7135 9963 10105 11983 12077 #Gaolei
		1018 4023 10004 11926 #Qinlian
		994 1038 1070 11963 11990 #Qiongya / Hainan
#Guangxi
		1023 7128 7168 10404 12407 #Guilin
		1597 4134 7095 7210 7650 10039 10431 12436 #Liujiang
		4028 4152 7044 7192 12023 #Cangwu
		4177 7137 10050#Nanning
		1469 4121 4656 #Zhennan
		1625 4077 4085 4628 10459 #Tiannan
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ 0.0 17.0 }
			no_phenomenon=0.920
			rain_light=0.050
			rain_heavy=0.030
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 0.0 19.0 }
			no_phenomenon=0.900
			rain_light=0.050
			rain_heavy=0.050
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 5.0 22.0 }
			no_phenomenon=0.820
			rain_light=0.050
			rain_heavy=0.130
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 11.0 27.0 }
			no_phenomenon=0.700
			rain_light=0.050
			rain_heavy=0.250
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 16.0 31.0 }
			no_phenomenon=0.600
			rain_light=0.200
			rain_heavy=0.400
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 19.0 32.0 }
			no_phenomenon=0.400
			rain_light=0.200
			rain_heavy=0.400
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 21.0 34.0 }
			no_phenomenon=0.200
			rain_light=0.300
			rain_heavy=0.500
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 21.0 34.0 }
			no_phenomenon=0.200
			rain_light=0.300
			rain_heavy=0.500
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 19.0 32.0 }
			no_phenomenon=0.300
			rain_light=0.200
			rain_heavy=0.500
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 14.0 28.0 }
			no_phenomenon=0.600
			rain_light=0.050
			rain_heavy=0.350
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 8.0 24.0 }
			no_phenomenon=0.900
			rain_light=0.050
			rain_heavy=0.050
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ 2.0 19.0 }
			no_phenomenon=0.950
			rain_light=0.050
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
