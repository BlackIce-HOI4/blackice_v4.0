
strategic_region={
	id=168
	name="STRATEGICREGION_168"
	provinces={
		5786 5812 5838 
	}
	naval_terrain = water_shallow_sea
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ 5.9 10.2 }
			no_phenomenon=0.250
			rain_light=0.177
			rain_heavy=0.006
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 7.1 12.0 }
			no_phenomenon=0.300
			rain_light=0.260
			rain_heavy=0.013
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 8.5 13.8 }
			no_phenomenon=0.350
			rain_light=0.095
			rain_heavy=0.024
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 12.6 15.4 }
			no_phenomenon=0.375
			rain_light=0.160
			rain_heavy=0.047
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 16.3 18.5 }
			no_phenomenon=0.375
			rain_light=0.095
			rain_heavy=0.057
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 18.1 22.1 }
			no_phenomenon=0.450
			rain_light=0.292
			rain_heavy=0.175
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 20.7 22.1 }
			no_phenomenon=0.500
			rain_light=0.075
			rain_heavy=0.045
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 20.8 23.9 }
			no_phenomenon=0.450
			rain_light=0.223
			rain_heavy=0.134
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 18.4 21.6 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.077
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 15.4 18.2 }
			no_phenomenon=0.325
			rain_light=0.297
			rain_heavy=0.107
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 11.6 13.9 }
			no_phenomenon=0.275
			rain_light=0.242
			rain_heavy=0.031
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ 7.1 11.5 }
			no_phenomenon=0.250
			rain_light=0.120
			rain_heavy=0.013
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
