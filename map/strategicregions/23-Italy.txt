
strategic_region={
	id=23
	name="STRATEGICREGION_23"
	provinces={
	 782 910 923 925 963 967 1616 3773 3885 3910 3923 3976 6606 6790 6793 6831 6862 6875 6926 6946 6985 9750 9752 9794 9838 9879 9904 9907 9924 11734 11751 11790 11833 11846 11861 11882 11889 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -8.4 20.1 }
			no_phenomenon=0.250
			rain_light=0.179
			rain_heavy=0.090
			snow=0.020
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ -7.7 21.4 }
			no_phenomenon=0.300
			rain_light=0.186
			rain_heavy=0.090
			snow=0.020
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ -4.5 24.6 }
			no_phenomenon=0.350
			rain_light=0.165
			rain_heavy=0.099
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ -0.9 28.0 }
			no_phenomenon=0.375
			rain_light=0.227
			rain_heavy=0.135
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 4.0 32.9 }
			no_phenomenon=0.375
			rain_light=0.179
			rain_heavy=0.107
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 9.0 37.6 }
			no_phenomenon=0.450
			rain_light=0.117
			rain_heavy=0.070
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 11.9 40.9 }
			no_phenomenon=0.500
			rain_light=0.076
			rain_heavy=0.046
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 12.3 41.1 }
			no_phenomenon=0.450
			rain_light=0.117
			rain_heavy=0.070
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 7.8 35.8 }
			no_phenomenon=0.375
			rain_light=0.199
			rain_heavy=0.119
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 3.2 30.5 }
			no_phenomenon=0.325
			rain_light=0.282
			rain_heavy=0.113
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ -2.0 24.8 }
			no_phenomenon=0.275
			rain_light=0.316
			rain_heavy=0.090
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -7.1 20.7 }
			no_phenomenon=0.250
			rain_light=0.247
			rain_heavy=0.090
			snow=0.020
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
