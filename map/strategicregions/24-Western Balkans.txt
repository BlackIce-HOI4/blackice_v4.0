
strategic_region={
	id=24
	name="STRATEGICREGION_24"
	provinces={
		591 596 606 624 665 953 982 984 3592 3596 3601 3627 3631 3654 3868 3924 3943 3974 3985 6611 6614 6619 6647 6650 6672 6799 6889 6942 6957 6983 9586 9588 9591 9595 9596 9611 9627 9894 9922 11564 11572 11574 11577 11581 11594 11612 11741 11816 11845 11872 11899 11901 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -9.5 16.2 }
			no_phenomenon=0.250
			rain_light=0.158
			rain_heavy=0.090
			snow=0.160
			blizzard=0.040
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ -8.6 18.1 }
			no_phenomenon=0.300
			rain_light=0.165
			rain_heavy=0.090
			snow=0.160
			blizzard=0.040
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ -4.8 22.1 }
			no_phenomenon=0.350
			rain_light=0.151
			rain_heavy=0.090
			snow=0.100
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 0.0 26.8 }
			no_phenomenon=0.375
			rain_light=0.179
			rain_heavy=0.090
			snow=0.050
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 5.5 32.0 }
			no_phenomenon=0.375
			rain_light=0.179
			rain_heavy=0.090
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 10.3 36.2 }
			no_phenomenon=0.450
			rain_light=0.186
			rain_heavy=0.090
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 12.6 38.8 }
			no_phenomenon=0.500
			rain_light=0.151
			rain_heavy=0.090
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 12.5 38.5 }
			no_phenomenon=0.450
			rain_light=0.165
			rain_heavy=0.099
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 7.2 32.8 }
			no_phenomenon=0.375
			rain_light=0.247
			rain_heavy=0.148
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 2.3 27.4 }
			no_phenomenon=0.325
			rain_light=0.234
			rain_heavy=0.135
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ -2.8 21.8 }
			no_phenomenon=0.275
			rain_light=0.289
			rain_heavy=0.173
			snow=0.170
			blizzard=0.030
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -8.0 17.0 }
			no_phenomenon=0.250
			rain_light=0.206
			rain_heavy=0.113
			snow=0.220
			blizzard=0.030
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
