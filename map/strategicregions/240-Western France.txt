strategic_region={
	id=240
	name="STRATEGICREGION_240"
	provinces={
		503 527 542 595 629 655 743 3495 3509 3523 3552 3659 3758 6536 6548 6572 6621 6657 6762 6777 9478 9495 9510 9525 9597 9631 9710 9737 9867 11463 11465 11508 11565 11582 11596 11600 11613 11616 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -7.7 17.8 }
			no_phenomenon=0.250
			rain_light=0.234
			rain_heavy=0.113
			snow=0.50
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ -7.9 19.0 }
			no_phenomenon=0.300
			rain_light=0.193
			rain_heavy=0.090
			snow=0.050
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ -5.4 22.1 }
			no_phenomenon=0.350
			rain_light=0.165
			rain_heavy=0.090
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ -3.2 24.8 }
			no_phenomenon=0.375
			rain_light=0.165
			rain_heavy=0.090
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 1.0 28.7 }
			no_phenomenon=0.375
			rain_light=0.179
			rain_heavy=0.090
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 4.6 32.1 }
			no_phenomenon=0.450
			rain_light=0.124
			rain_heavy=0.074
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 6.4 34.5 }
			no_phenomenon=0.500
			rain_light=0.124
			rain_heavy=0.074
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 6.4 34.6 }
			no_phenomenon=0.450
			rain_light=0.124
			rain_heavy=0.074
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 3.3 31.5 }
			no_phenomenon=0.375
			rain_light=0.179
			rain_heavy=0.107
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 0.3 26.2 }
			no_phenomenon=0.325
			rain_light=0.261
			rain_heavy=0.157
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ -4.2 21.5 }
			no_phenomenon=0.275
			rain_light=0.247
			rain_heavy=0.148
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -7.1 18.3 }
			no_phenomenon=0.250
			rain_light=0.261
			rain_heavy=0.157
			snow=0.050
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
