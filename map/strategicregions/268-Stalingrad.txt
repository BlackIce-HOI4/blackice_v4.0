strategic_region={
	id=268
	name="STRATEGICREGION_268"
	provinces={
		449 484 487 498 500 509 518 520 533 544 579 3431 3478 3489 3503 3505 3517 3518 3519 3529 3539 3540 3555 3582 6454 6479 6503 6506 6527 6528 6541 6544 6563 6566 6575 6602 9436 9442 9459 9464 9474 9475 9488 9502 9504 9516 9530 9540 9565 11395 11423 11447 11451 11459 11474 11476 11484 11485 11501 11512 11527 11555 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -23.1 4.3 }
			no_phenomenon=0.250
			rain_light=0.148
			rain_heavy=0.089
			snow=0.600
			blizzard=0.200
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ -22.7 5.4 }
			no_phenomenon=0.300
			rain_light=0.124
			rain_heavy=0.074
			snow=0.550
			blizzard=0.200
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ -13.9 12.5 }
			no_phenomenon=0.350
			rain_light=0.140
			rain_heavy=0.084
			snow=0.400
			blizzard=0.100
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ -3.9 23.6 }
			no_phenomenon=0.375
			rain_light=0.110
			rain_heavy=0.066
			snow=0.300
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 5.9 32.0 }
			no_phenomenon=0.375
			rain_light=0.203
			rain_heavy=0.122
			snow=0.100
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 10.8 37.1 }
			no_phenomenon=0.450
			rain_light=0.121
			rain_heavy=0.073
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 14.4 39.3 }
			no_phenomenon=0.500
			rain_light=0.146
			rain_heavy=0.088
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 13.2 39.1 }
			no_phenomenon=0.450
			rain_light=0.055
			rain_heavy=0.033
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 5.9 31.1 }
			no_phenomenon=0.375
			rain_light=0.099
			rain_heavy=0.059
			snow=0.100
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ -3.3 21.5 }
			no_phenomenon=0.325
			rain_light=0.091
			rain_heavy=0.055
			snow=0.300
			blizzard=0.000
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ -11.4 12.5 }
			no_phenomenon=0.275
			rain_light=0.069
			rain_heavy=0.041
			snow=0.500
			blizzard=0.200
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -17.2 8.3 }
			no_phenomenon=0.250
			rain_light=0.116
			rain_heavy=0.070
			snow=0.600
			blizzard=0.200
			arctic_water=0.000
			mud=2.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
