
strategic_region={
	id=272
	name="STRATEGICREGION_272"
	provinces={
		3590 8340
	}
	naval_terrain=water_shallow_sea
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -1.6 5.0 }
			no_phenomenon=0.250
			rain_light=0.177
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ -0.2 6.4 }
			no_phenomenon=0.300
			rain_light=0.260
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 0.2 7.8 }
			no_phenomenon=0.350
			rain_light=0.095
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 4.4 7.8 }
			no_phenomenon=0.375
			rain_light=0.160
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 7.6 12.0 }
			no_phenomenon=0.375
			rain_light=0.095
			rain_heavy=0.004
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 10.8 14.8 }
			no_phenomenon=0.450
			rain_light=0.292
			rain_heavy=0.012
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 12.6 14.8 }
			no_phenomenon=0.500
			rain_light=0.075
			rain_heavy=0.012
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 12.9 16.2 }
			no_phenomenon=0.450
			rain_light=0.223
			rain_heavy=0.019
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 11.8 13.4 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.007
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 8.6 9.2 }
			no_phenomenon=0.325
			rain_light=0.297
			rain_heavy=0.001
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 5.4 6.4 }
			no_phenomenon=0.275
			rain_light=0.242
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -0.2 5.0 }
			no_phenomenon=0.250
			rain_light=0.120
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
