
strategic_region={
	id=283
	name="STRATEGICREGION_283"
	provinces={
		
	}
	naval_terrain = water_shallow_sea
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -5.3 2.2 }
			no_phenomenon=0.250
			rain_light=0.177
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ -4.2 3.3 }
			no_phenomenon=0.300
			rain_light=0.260
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ -3.6 4.4 }
			no_phenomenon=0.350
			rain_light=0.095
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ -0.3 4.4 }
			no_phenomenon=0.375
			rain_light=0.160
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 2.5 7.8 }
			no_phenomenon=0.375
			rain_light=0.095
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 5.2 10.0 }
			no_phenomenon=0.450
			rain_light=0.292
			rain_heavy=0.001
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 6.9 10.0 }
			no_phenomenon=0.500
			rain_light=0.075
			rain_heavy=0.001
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 6.9 11.2 }
			no_phenomenon=0.450
			rain_light=0.223
			rain_heavy=0.001
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 5.8 8.9 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 3.1 5.6 }
			no_phenomenon=0.325
			rain_light=0.297
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 0.3 3.3 }
			no_phenomenon=0.275
			rain_light=0.242
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -4.2 2.2 }
			no_phenomenon=0.250
			rain_light=0.120
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
