
strategic_region={
	id=38
	name="STRATEGICREGION_38"
	provinces={
		17 243 279 334 362 388 389 493 584 3295 3324 3381 3460 3532 6558 9263 9439 9508 9532 9546 11232 11428 11515 11558 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -15.9 10.0 }
			no_phenomenon=0.250
			rain_light=0.089
			rain_heavy=0.053
			snow=0.200
			blizzard=0.050
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ -15.3 11.2 }
			no_phenomenon=0.300
			rain_light=0.076
			rain_heavy=0.046
			snow=0.150
			blizzard=0.050
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ -12.2 15.4 }
			no_phenomenon=0.350
			rain_light=0.096
			rain_heavy=0.058
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ -7.5 22.0 }
			no_phenomenon=0.375
			rain_light=0.083
			rain_heavy=0.050
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ -2.1 27.2 }
			no_phenomenon=0.375
			rain_light=0.151
			rain_heavy=0.090
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 2.5 30.6 }
			no_phenomenon=0.450
			rain_light=0.165
			rain_heavy=0.099
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 5.3 33.0 }
			no_phenomenon=0.500
			rain_light=0.227
			rain_heavy=0.136
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 4.9 32.7 }
			no_phenomenon=0.450
			rain_light=0.172
			rain_heavy=0.103
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 0.3 27.5 }
			no_phenomenon=0.375
			rain_light=0.151
			rain_heavy=0.090
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ -4.9 21.1 }
			no_phenomenon=0.325
			rain_light=0.124
			rain_heavy=0.074
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ -9.5 14.9 }
			no_phenomenon=0.275
			rain_light=0.110
			rain_heavy=0.066
			snow=0.200
			blizzard=0.050
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -13.7 11.1 }
			no_phenomenon=0.250
			rain_light=0.103
			rain_heavy=0.062
			snow=0.250
			blizzard=0.050
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
