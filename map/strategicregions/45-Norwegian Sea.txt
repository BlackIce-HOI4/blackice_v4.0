
strategic_region={
	id=45
	name="STRATEGICREGION_45"
	provinces={
		2446 2499 2568 2578 2695 2936 2960 2986 3038 4034 5792 5818 5845 5895 5966 8530 8588 8640 8666 8692 8717 8743 8767 8789 8973 9023 
	}
	naval_terrain = water_shallow_sea
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -9.0 1.0 }
			no_phenomenon=0.250
			rain_light=0.177
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ -8.0 2.0 }
			no_phenomenon=0.300
			rain_light=0.260
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ -7.0 3.0 }
			no_phenomenon=0.350
			rain_light=0.095
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ -4.0 3.0 }
			no_phenomenon=0.375
			rain_light=0.160
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ -1.0 6.0 }
			no_phenomenon=0.375
			rain_light=0.095
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 2.0 8.0 }
			no_phenomenon=0.450
			rain_light=0.292
			rain_heavy=0.001
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 4.0 8.0 }
			no_phenomenon=0.500
			rain_light=0.075
			rain_heavy=0.001
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 3.5 9.0 }
			no_phenomenon=0.450
			rain_light=0.223
			rain_heavy=0.001
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 2.0 7.0 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ -1.0 4.0 }
			no_phenomenon=0.325
			rain_light=0.297
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ -4.0 2.0 }
			no_phenomenon=0.275
			rain_light=0.242
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -8.0 1.0 }
			no_phenomenon=0.250
			rain_light=0.120
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
