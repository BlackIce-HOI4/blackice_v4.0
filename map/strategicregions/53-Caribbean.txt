strategic_region={
	id=53
	name="STRATEGICREGION_53"
	provinces={
		177 379 1440 1513 1550 1594 1635 2110 2232 2258 2284 2500 2547 2571 2597 2766 2791 2863 2865 2887 2910 2934 3284 4155 4443 4450 4476 4598 4600 4641 4667 5507 5533 5557 5581 5607 5633 5658 5683 5700 5709 5735 5761 5788 5814 5840 7123 7230 7451 7456 7546 7590 7622 7632 7660 9377 10374 10440 10484 10498 11106 11350 12304 12347 12477 13009 13012 13084 
	}
	naval_terrain=water_fjords
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ 20.8 25.2 }
			no_phenomenon=0.250
			rain_light=0.177
			rain_heavy=0.106
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 21.8 26.6 }
			no_phenomenon=0.300
			rain_light=0.260
			rain_heavy=0.155
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 22.3 28.1 }
			no_phenomenon=0.350
			rain_light=0.095
			rain_heavy=0.057
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 23.7 29.5 }
			no_phenomenon=0.375
			rain_light=0.160
			rain_heavy=0.096
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 24.2 29.5 }
			no_phenomenon=0.375
			rain_light=0.095
			rain_heavy=0.057
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 25.6 31.9 }
			no_phenomenon=0.450
			rain_light=0.292
			rain_heavy=0.175
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 30.5 31.9 }
			no_phenomenon=0.500
			rain_light=0.075
			rain_heavy=0.045
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 29.3 35.3 }
			no_phenomenon=0.450
			rain_light=0.223
			rain_heavy=0.134
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 28.5 31.4 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.077
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 26.1 29.0 }
			no_phenomenon=0.325
			rain_light=0.297
			rain_heavy=0.178
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 22.7 27.1 }
			no_phenomenon=0.275
			rain_light=0.242
			rain_heavy=0.145
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ 19.8 25.2 }
			no_phenomenon=0.250
			rain_light=0.120
			rain_heavy=0.072
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
