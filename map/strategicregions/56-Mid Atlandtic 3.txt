strategic_region={
	id=56
	name="STRATEGICREGION_56"
	provinces={
		1706 2312 2336 2425 2449 2474 2944 4707 4930 5141 5251 5276 5303 5313 5329 5352 5362 5399 5423 5904 5927 5976 5999 6053 8273 8299 
	}
	naval_terrain=water_deep_ocean
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -1.4 5.8 }
			no_phenomenon=0.250
			rain_light=0.177
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ -0.4 7.2 }
			no_phenomenon=0.300
			rain_light=0.260
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 1.0 8.6 }
			no_phenomenon=0.350
			rain_light=0.095
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 4.4 9.6 }
			no_phenomenon=0.375
			rain_light=0.160
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 7.8 12.4 }
			no_phenomenon=0.375
			rain_light=0.095
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 10.0 15.2 }
			no_phenomenon=0.450
			rain_light=0.292
			rain_heavy=0.001
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 12.4 15.2 }
			no_phenomenon=0.500
			rain_light=0.075
			rain_heavy=0.001
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 12.1 16.6 }
			no_phenomenon=0.450
			rain_light=0.223
			rain_heavy=0.001
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 10.0 14.6 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 7.0 11.6 }
			no_phenomenon=0.325
			rain_light=0.297
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 3.6 8.4 }
			no_phenomenon=0.275
			rain_light=0.242
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -0.4 6.6 }
			no_phenomenon=0.250
			rain_light=0.120
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
