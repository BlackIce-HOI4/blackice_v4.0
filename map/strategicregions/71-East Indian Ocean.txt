strategic_region={
	id=71
	name="STRATEGICREGION_71"
	provinces={
		29 235 436 617 812 1017 1462 1687 1912 2125 2979 3023 3222 8141 8267 8293 8319 8344 8372 8397 8422 8441 8446 8468 8473 8500 8526 8549 8573 8596 8624 8626 8650 8671 8677 8679 8697 8704 8730 8752 8754 8771 8775 8777 8792 8798 8800 8818 8823 8825 8849 8875 8899 8917 8922 8924 8946 8970 8994 9018 9058 13002 
	}
	naval_terrain=water_deep_ocean
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ 28.4 29.7 }
			no_phenomenon=0.250
			rain_light=0.177
			rain_heavy=0.106
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 27.2 32.9 }
			no_phenomenon=0.300
			rain_light=0.260
			rain_heavy=0.155
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 26.6 29.2 }
			no_phenomenon=0.350
			rain_light=0.095
			rain_heavy=0.057
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 24.3 27.0 }
			no_phenomenon=0.375
			rain_light=0.160
			rain_heavy=0.096
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 21.2 25.2 }
			no_phenomenon=0.375
			rain_light=0.095
			rain_heavy=0.057
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 18.4 23.4 }
			no_phenomenon=0.450
			rain_light=0.292
			rain_heavy=0.175
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 19.4 23.4 }
			no_phenomenon=0.500
			rain_light=0.075
			rain_heavy=0.045
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 20.2 24.8 }
			no_phenomenon=0.450
			rain_light=0.223
			rain_heavy=0.134
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 20.7 26.1 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.077
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 22.1 27.4 }
			no_phenomenon=0.325
			rain_light=0.297
			rain_heavy=0.178
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 22.5 27.4 }
			no_phenomenon=0.275
			rain_light=0.242
			rain_heavy=0.145
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ 23.9 29.7 }
			no_phenomenon=0.250
			rain_light=0.120
			rain_heavy=0.072
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
