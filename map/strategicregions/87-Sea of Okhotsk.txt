
strategic_region={
	id=87
	name="STRATEGICREGION_87"
	provinces={
		220 1322 1399 1515 1820 1899 2436 2460 2485 2560 2808 2881 2903 2927 2951 3424 3615 3808 4015 4239 4688 4911 5126 5243 5268 5295 5321 5344 5391 5415 5439 5488 5514 5588 5614 8893 12446 13037 13258 
	}
	#naval_terrain = ocean
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -7.9 0.9 }
			no_phenomenon=0.250
			rain_light=0.177
			rain_heavy=0.000
			snow=0.250
			blizzard=0.150
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ -7.0 1.8 }
			no_phenomenon=0.300
			rain_light=0.260
			rain_heavy=0.000
			snow=0.250
			blizzard=0.100
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ -6.1 2.6 }
			no_phenomenon=0.350
			rain_light=0.095
			rain_heavy=0.000
			snow=0.250
			blizzard=0.050
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ -3.5 2.6 }
			no_phenomenon=0.375
			rain_light=0.160
			rain_heavy=0.000
			snow=0.150
			blizzard=0.050
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ -0.9 5.2 }
			no_phenomenon=0.375
			rain_light=0.095
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 1.8 7.0 }
			no_phenomenon=0.450
			rain_light=0.292
			rain_heavy=0.001
			snow=0.000
			blizzard=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 3.5 7.0 }
			no_phenomenon=0.500
			rain_light=0.075
			rain_heavy=0.001
			snow=0.000
			blizzard=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 3.1 7.9 }
			no_phenomenon=0.450
			rain_light=0.223
			rain_heavy=0.001
			snow=0.000
			blizzard=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 1.8 6.1 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.000
			snow=0.000
			blizzard=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ -0.9 3.5 }
			no_phenomenon=0.325
			rain_light=0.297
			rain_heavy=0.000
			snow=0.150
			blizzard=0.000
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ -3.5 1.8 }
			no_phenomenon=0.275
			rain_light=0.242
			rain_heavy=0.000
			snow=0.250
			blizzard=0.100
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -7.0 0.9 }
			no_phenomenon=0.250
			rain_light=0.120
			rain_heavy=0.000
			snow=0.300
			blizzard=0.100
			mud=0.500
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}