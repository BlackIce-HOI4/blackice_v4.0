strategic_region={
	id=92
	name="STRATEGICREGION_92"
	provinces={
		28 234 435 616 811 1016 1235 1461 1686 1911 2291 2317 2341 4676 5116 5636 9057 
	}
	#naval_terrain = ocean
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ 29.1 30.4 }
			no_phenomenon=0.250
			rain_light=0.177
			rain_heavy=0.106
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ 27.9 33.7 }
			no_phenomenon=0.300
			rain_light=0.260
			rain_heavy=0.155
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ 27.2 30.0 }
			no_phenomenon=0.350
			rain_light=0.095
			rain_heavy=0.057
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ 24.9 27.7 }
			no_phenomenon=0.375
			rain_light=0.160
			rain_heavy=0.096
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ 21.7 25.8 }
			no_phenomenon=0.375
			rain_light=0.095
			rain_heavy=0.057
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ 18.9 24.0 }
			no_phenomenon=0.450
			rain_light=0.292
			rain_heavy=0.175
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ 19.8 24.0 }
			no_phenomenon=0.500
			rain_light=0.075
			rain_heavy=0.045
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ 20.8 25.4 }
			no_phenomenon=0.450
			rain_light=0.223
			rain_heavy=0.134
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ 21.2 26.8 }
			no_phenomenon=0.375
			rain_light=0.128
			rain_heavy=0.077
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ 22.6 28.1 }
			no_phenomenon=0.325
			rain_light=0.297
			rain_heavy=0.178
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ 23.1 28.1 }
			no_phenomenon=0.275
			rain_light=0.242
			rain_heavy=0.145
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ 24.4 30.4 }
			no_phenomenon=0.250
			rain_light=0.120
			rain_heavy=0.072
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=1.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
