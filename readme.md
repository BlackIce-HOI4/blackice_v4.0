![Black ICE](BICE_modfile/Bice%20logos/BlackICE logo 2 mtg.png "Black ICE")
# Black ICE
A total overhaul mod for Hearts of Iron IV
## Branches
### Active Branches 
 - [DEV](https://gitlab.com/BlackIce-HOI4/blackice_v4.0/-/tree/DEV)
 - [aquillo2](https://gitlab.com/BlackIce-HOI4/blackice_v4.0/-/tree/aquillo2)
 - [MAV01](https://gitlab.com/BlackIce-HOI4/blackice_v4.0/-/tree/MAV01)
 - [Romania_Rework](https://gitlab.com/BlackIce-HOI4/blackice_v4.0/-/tree/Romania_Rework)
 - [legorin1](https://gitlab.com/BlackIce-HOI4/blackice_v4.0/-/tree/legorin1)
 - [machotacoman1](https://gitlab.com/BlackIce-HOI4/blackice_v4.0/-/tree/machotacoman1)
 - [EloudaNaval](https://gitlab.com/BlackIce-HOI4/blackice_v4.0/-/tree/EloudaNaval)
 - [aquillo1](https://gitlab.com/BlackIce-HOI4/blackice_v4.0/-/tree/aquillo1)
 - [MERGE](https://gitlab.com/BlackIce-HOI4/blackice_v4.0/-/tree/MERGE)
 - [interface_overhaul](https://gitlab.com/BlackIce-HOI4/blackice_v4.0/-/tree/interface_overhaul)
## Current BICE 5.0 Team
Current - Devs:
- [Maverick87](https://gitlab.com/maverick87); [ColonialRebel](https://gitlab.com/ColonialRebel); [Jack](https://gitlab.com./Jack-Bice); [Aquillo](https://gitlab.com/aquillo); [Elouda](https://gitlab.com/Elouda); [Machotacoman]();

Honour Dev:
- [Panzeroo](https://gitlab.com/Panzeroo)

BICE - Team:
- [Giubbi](https://gitlab.com/giubbi); [Doruk](https://gitlab.com/DorukSega); [Jango](https://gitlab.com/comradejango32); elCirri; Runsondiesel; Darthkenson; Erwin78; [Chemical Art](https://gitlab.com/chemicalart); [Trylun](https://gitlab.com/Trylun);
- Dramien; Warhammer; Meridio; Eric; Franc007She; Yagashura; Polarace; DasWiking; Jaden; Jay R

with thanks to:
Energico, Msslupu, duel_obliteration, yuudachipoi, George Parr, Maulwurf, ryjz, pxroberto, ichatv,
franc sher, erwin78, verenikin, Silvercloud, Stjern, Alkin, Aranax, Artur, atheory, Cassian, HoI4 Modding Coop, #
Thedeciderr, Comrademisha, Deadend85, Mackman113, Oumajgad, Timasaurus007,
solfall, Sunwhisper, Wunder wuffle, Xxroxx, Alex Kamal, Spaceraider, Indyclone77, Jango, Andret, Grenight
Belgian Panzer, Warhammer, MisterJay
